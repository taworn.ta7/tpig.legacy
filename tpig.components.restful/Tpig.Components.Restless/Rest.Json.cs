﻿using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Tpig.Components.Restless {
    public partial class Rest {

        /// <summary>
        /// Call JSON service, async version.
        /// </summary>
        public virtual async Task<bool> JsonAsync<TRequest, TResponse>(Call<TRequest, TResponse> call)
            where TRequest : Request
            where TResponse : Response {
            // serialize request object to JSON
            call.Request.BeforeRequest(call);
            var input = (string)null;
            if (call.LogRequest == LogOptions.FORMAT) {
                input = JsonSerializer.Serialize(call.Request, new JsonSerializerOptions { WriteIndented = true });
                logger.Log(call.LogLevel, "request: {0}", input);
            }
            else if (call.LogRequest == LogOptions.NORMAL) {
                input = JsonSerializer.Serialize(call.Request, new JsonSerializerOptions { });
                logger.Log(call.LogLevel, "request: {0}", input);
            }
            else
                input = JsonSerializer.Serialize(call.Request, new JsonSerializerOptions { });
            call.RawRequest = new StringContent(input, Encoding.UTF8, "application/json");

            // send and await...
            var response = await SendAsync(call);
            if (response == null)
                return false;

            // receiving data...
            var data = await ReceivingAsync(call, response);

            // check result
            if (data == null)
                return false;
            if (!ReceivedJson(call, data))
                return false;
            return call.Response != null;
        }

        private bool ReceivedJson<TRequest, TResponse>(Call<TRequest, TResponse> call, string response)
            where TRequest : Request
            where TResponse : Response {
            if (call.LogResponseRaw)
                logger.Log(call.LogLevel, "response raw: {0}", response);

            try {
                call.Response = JsonSerializer.Deserialize<TResponse>(response);
                if (call.Response != null) {
                    // content from response is JSON, everything is ok
                    call.Response.AfterResponse(call);
                    if (call.LogResponse == LogOptions.FORMAT) {
                        var output = JsonSerializer.Serialize(call.Response, new JsonSerializerOptions { WriteIndented = true });
                        logger.Log(call.LogLevel, "response: {0}", output);
                    }
                    else if (call.LogResponse == LogOptions.NORMAL) {
                        var output = JsonSerializer.Serialize(call.Response, new JsonSerializerOptions { });
                        logger.Log(call.LogLevel, "response: {0}", output);
                    }
                    return true;
                }
            }
            catch (JsonException ex) {
                logger.Log(call.ErrorLogLevel, "{0}: {1}", ex.GetType(), ex.Message);
                return false;
            }
            catch (Exception ex) {
                logger.Log(call.ErrorLogLevel, ex);
                return false;
            }

            // response is not JSON
            return false;
        }

        // ----------------------------------------------------------------------

        /// <summary>
        /// Call JSON service, sync version.
        /// </summary>
        public virtual bool Json<TRequest, TResponse>(Call<TRequest, TResponse> call)
            where TRequest : Request
            where TResponse : Response {
            return Task.Run(async () => await JsonAsync(call)).Result;
        }

        // ----------------------------------------------------------------------

        /// <summary>
        /// Call JSON service, safe & sync version.
        /// </summary>
        public virtual bool SafeJson<TRequest, TResponse>(Call<TRequest, TResponse> call)
            where TRequest : Request
            where TResponse : Response {
            try {
                return Task.Run(async () => await JsonAsync(call)).Result;
            }
            catch (Exception ex) {
                logger.Log(call.ErrorLogLevel, ex);
            }
            return false;
        }

    }
}
