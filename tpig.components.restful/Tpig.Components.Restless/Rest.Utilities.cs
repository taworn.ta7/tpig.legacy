﻿namespace Tpig.Components.Restless {
    public partial class Rest {

        // base class for Request
        public class Request {
            public virtual void BeforeRequest<TRequest, TResponse>(Call<TRequest, TResponse> call)
                where TRequest : Request
                where TResponse : Response {
            }
        }

        // base class for Response
        public class Response {
            public virtual void AfterResponse<TRequest, TResponse>(Call<TRequest, TResponse> call)
                where TRequest : Request
                where TResponse : Response {
            }
        }

    }
}
