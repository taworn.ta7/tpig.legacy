﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;

namespace Tpig.Components.Restless {
    public partial class Rest {

        /// <summary>
        /// Handle error function.
        /// </summary>
        /// <returns>Return true if it handle.  Otherwise, it is false.</returns>
        public bool HandleError<TRequest, TResponse>(Call<TRequest, TResponse> call, Exception x)
            where TRequest : Request
            where TResponse : Response {
            if (x is HttpRequestException) {
                var xx = x.InnerException;
                if (xx is WebException) {
                    var xxx = xx.InnerException;
                    if (xxx is SocketException)
                        logger.Log(call.ErrorLogLevel, "{0}: {1}", xxx.GetType(), xxx.Message);
                    else
                        logger.Log(call.ErrorLogLevel, "{0}: {1}", xx.GetType(), xx.Message);
                }
                else
                    logger.Log(call.ErrorLogLevel, "{0}: {1}", x.GetType(), x.Message);
                return true;
            }
            else
                return false;
        }

    }
}
