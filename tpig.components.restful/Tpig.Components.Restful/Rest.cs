﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using NLog;

namespace Tpig.Components.Restful {
    /// <summary>
    /// A simple RESTful web service.
    /// </summary>
    public partial class Rest {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Constructor.
        /// </summary>
        public Rest(string baseUrl = null) {
            BaseUrl = baseUrl;
        }

        /// <summary>
        /// Base URL.  Can be null, but have to set full URL all times.
        /// </summary>
        public string BaseUrl { get; set; } = null;

        /// <summary>
        /// Global timeout.
        /// </summary>
        public TimeSpan TimeOut { get; set; } = new TimeSpan(0, 0, 60);

        // ----------------------------------------------------------------------

        // method
        public enum Method {
            GET,
            POST,
            PUT,
            DELETE
        }

        // log options
        public enum LogOptions {
            NORMAL,
            FORMAT,
            DISABLE
        }

        /// <summary>
        /// Data to send request.
        /// </summary>
        public class Call<TRequest, TResponse>
            where TRequest : Request
            where TResponse : Response {

            // URL
            public string Url { get; set; } = null;

            // method to call: GET, POST, PUT and DELETE
            public Method Method { get; set; } = Method.GET;

            // timeout
            public TimeSpan? TimeOut { get; set; } = null;

            // cookies to send along with request
            public CookieContainer CookieContainer { get; set; } = null;

            // headers to send along with request
            public Dictionary<string, string> Headers { get; set; } = null;

            // request to send
            public TRequest Request { get; set; } = null;
            public StringContent RawRequest { get; set; } = null;

            // response to receive
            public string RawResponse { get; set; } = null;
            public TResponse Response { get; set; } = null;

            // status returned from server
            public HttpStatusCode? StatusCode { get; set; } = null;

            // both log levels for normal log and error log
            public LogLevel LogLevel { get; set; } = LogLevel.Trace;
            public LogLevel ErrorLogLevel { get; set; } = LogLevel.Error;

            // additional for log flags
            public bool LogStatus { get; set; } = true;
            public LogOptions LogRequest { get; set; } = LogOptions.NORMAL;
            public LogOptions LogResponse { get; set; } = LogOptions.NORMAL;
            public bool LogResponseRaw { get; set; } = false;

        }

    }
}
