﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Tpig.Components.Restful {
    public partial class Rest {

        /// <summary>
        /// Send HTTP async.
        /// </summary>
        public virtual async Task<HttpResponseMessage> SendAsync<TRequest, TResponse>(Call<TRequest, TResponse> call)
            where TRequest : Request
            where TResponse : Response {
            if (!call.Url.StartsWith("http://") && !call.Url.StartsWith("https://")) {
                var uri = new Uri(new Uri(BaseUrl), call.Url);
                call.Url = uri.AbsoluteUri;
            }
            if (call.LogStatus)
                logger.Log(call.LogLevel, "service: {0} {1}", call.Method, call.Url);

            if (call.CookieContainer == null)
                call.CookieContainer = new CookieContainer();

            using (var handler = new HttpClientHandler() { CookieContainer = call.CookieContainer }) {
                using (var client = new HttpClient(handler)) {
                    if (call.TimeOut.HasValue)
                        client.Timeout = call.TimeOut.Value;
                    /*else if (TimeOut != null)
                        client.Timeout = TimeOut;*/

                    if (call.Headers != null && call.Headers.Count > 0) {
                        foreach (var item in call.Headers) {
                            client.DefaultRequestHeaders.Add(item.Key, item.Value);
                        }
                    }

                    try {
                        switch (call.Method) {
                            default:
                            case Method.GET:
                                return await client.GetAsync(call.Url);

                            case Method.POST:
                                return await client.PostAsync(call.Url, call.RawRequest);

                            case Method.PUT:
                                return await client.PutAsync(call.Url, call.RawRequest);

                            case Method.DELETE:
                                return await client.DeleteAsync(call.Url);
                        }
                    }
                    catch (HttpRequestException ex) {
                        if (!HandleError(call, ex))
                            throw;
                    }
                    catch (WebException ex) {
                        if (!HandleError(call, ex))
                            throw;
                    }
                    catch (SocketException ex) {
                        if (!HandleError(call, ex))
                            throw;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Receiving HTTP async.
        /// </summary>
        public virtual async Task<string> ReceivingAsync<TRequest, TResponse>(Call<TRequest, TResponse> call, HttpResponseMessage response)
            where TRequest : Request
            where TResponse : Response {
            try {
                if (response != null) {
                    call.StatusCode = response.StatusCode;
                    if (call.LogStatus)
                        logger.Log(call.LogLevel, "service: status={0} {1}", (int)call.StatusCode, call.StatusCode); ;

                    // check output
                    if (response.IsSuccessStatusCode) {
                        // response status is 200, try get content from response
                        return await response.Content.ReadAsStringAsync();
                    }
                }
            }
            catch (Exception ex) {
                logger.Log(call.ErrorLogLevel, "{0}: {1}", ex.GetType(), ex.Message);
            }

            // response status is NOT 200
            return null;
        }

    }
}
