﻿using Newtonsoft.Json;
using NLog;
using Tpig.Components.Restful;

namespace Tpig.Examples.Testful.Services {
    public class Hello {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public const string Service = "hello";

        // ----------------------------------------------------------------------

        public class Request : Common.Request {
        }

        // ----------------------------------------------------------------------

        public class Response : Common.Response {
            [JsonProperty("message")]
            public string Message { get; set; }
        }

        // ----------------------------------------------------------------------

        public static void Test(Rest rest) {
            logger.Info("Test '{0}'", Service);
            rest.SafeJson(new Rest.Call<Request, Response> {
                Url = Service,
                Method = Rest.Method.GET,
                Request = new Request {
                }
            });
        }

    }
}
