﻿using Newtonsoft.Json;
using Tpig.Components.Restful;

namespace Tpig.Examples.Testful.Services {
    public class Common {

        public class Request : Rest.Request {
        }

        // ----------------------------------------------------------------------

        public class Response : Rest.Response {
            [JsonProperty("ok")]
            public int Ok { get; set; } = 0;
        }

    }
}
