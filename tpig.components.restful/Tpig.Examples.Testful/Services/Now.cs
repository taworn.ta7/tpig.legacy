﻿using Newtonsoft.Json;
using NLog;
using Tpig.Components.Restful;

namespace Tpig.Examples.Testful.Services {
    public class Now {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public const string Service = "now";

        // ----------------------------------------------------------------------

        public class Request : Common.Request {
        }

        // ----------------------------------------------------------------------

        public class Response : Common.Response {
            [JsonProperty("now")]
            public string Now { get; set; }
        }

        // ----------------------------------------------------------------------

        public static void Test(Rest rest) {
            logger.Info("Test '{0}'", Service);
            logger.Info("change LogLevel and ErrorLogLevel");
            rest.SafeJson(new Rest.Call<Request, Response> {
                Url = Service,
                Method = Rest.Method.GET,
                Request = new Request {
                },
                LogLevel = LogLevel.Debug,
                ErrorLogLevel = LogLevel.Warn
            });
        }

    }
}
