﻿using Tpig.Components.Restful;
using Tpig.Examples.Testful.Services;

namespace Tpig.Examples.Testful {
    class Program {
        static void Main(string[] args) {
            var rest = new Rest();
            rest.BaseUrl = "http://localhost:55555/";
            Hello.Test(rest);
            Now.Test(rest);
            Mirror.Test0(rest);
            Mirror.Test1(rest);
        }
    }
}
