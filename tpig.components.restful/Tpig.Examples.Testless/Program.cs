﻿using Tpig.Components.Restless;
using Tpig.Examples.Testless.Services;

namespace Tpig.Examples.Testless {
    class Program {
        static void Main(string[] args) {
            var rest = new Rest();
            rest.BaseUrl = "http://localhost:55555/";
            Hello.Test(rest);
            Now.Test(rest);
            Mirror.Test0(rest);
            Mirror.Test1(rest);
        }
    }
}
