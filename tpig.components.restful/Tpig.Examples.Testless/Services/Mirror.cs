﻿using System.Text.Json.Serialization;
using NLog;
using Tpig.Components.Restless;

namespace Tpig.Examples.Testless.Services {
    public class Mirror {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public const string Service = "mirror";

        // ----------------------------------------------------------------------

        public class Request : Common.Request {
            [JsonPropertyName("mirror")]
            public string Mirror { get; set; }
        }

        // ----------------------------------------------------------------------

        public class Response : Common.Response {
            [JsonPropertyName("mirror")]
            public string Mirror { get; set; }
        }

        // ----------------------------------------------------------------------

        public static void Test0(Rest rest) {
            logger.Info("Test '{0}'", Service);
            logger.Info("change LogOptions");
            rest.SafeJson(new Rest.Call<Request, Response>() {
                Url = Service,
                Method = Rest.Method.POST,
                Request = new Request {
                    Mirror = "The Mirror"
                },
                LogRequest = Rest.LogOptions.FORMAT,
                LogResponse = Rest.LogOptions.DISABLE,
                LogResponseRaw = true
            });
        }

        public static void Test1(Rest rest) {
            logger.Info("Test '{0}'", Service);
            logger.Info("no log output");
            rest.SafeJson(new Rest.Call<Request, Response>() {
                Url = Service,
                Method = Rest.Method.POST,
                Request = new Request {
                    Mirror = "Null"
                },
                LogStatus = false,
                LogRequest = Rest.LogOptions.DISABLE,
                LogResponse = Rest.LogOptions.DISABLE,
                LogResponseRaw = false
            });
        }

    }
}
