﻿using System.Text.Json.Serialization;
using NLog;
using Tpig.Components.Restless;

namespace Tpig.Examples.Testless.Services {
    public class Hello {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public const string Service = "hello";

        // ----------------------------------------------------------------------

        public class Request : Common.Request {
        }

        // ----------------------------------------------------------------------

        public class Response : Common.Response {
            [JsonPropertyName("message")]
            public string Message { get; set; }
        }

        // ----------------------------------------------------------------------

        public static void Test(Rest rest) {
            logger.Info("Test '{0}'", Service);
            rest.SafeJson(new Rest.Call<Request, Response> {
                Url = Service,
                Method = Rest.Method.GET,
                Request = new Request {
                }
            });
        }

    }
}
