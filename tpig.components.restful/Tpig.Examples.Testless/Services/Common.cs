﻿using System.Text.Json.Serialization;
using Tpig.Components.Restless;

namespace Tpig.Examples.Testless.Services {
    public class Common {

        public class Request : Rest.Request {
        }

        // ----------------------------------------------------------------------

        public class Response : Rest.Response {
            [JsonPropertyName("ok")]
            public int Ok { get; set; } = 0;
        }

    }
}
