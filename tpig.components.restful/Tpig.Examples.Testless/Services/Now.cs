﻿using System.Text.Json.Serialization;
using NLog;
using Tpig.Components.Restless;

namespace Tpig.Examples.Testless.Services {
    public class Now {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public const string Service = "now";

        // ----------------------------------------------------------------------

        public class Request : Common.Request {
        }

        // ----------------------------------------------------------------------

        public class Response : Common.Response {
            [JsonPropertyName("now")]
            public string Now { get; set; }
        }

        // ----------------------------------------------------------------------

        public static void Test(Rest rest) {
            logger.Info("Test '{0}'", Service);
            logger.Info("change LogLevel and ErrorLogLevel");
            rest.SafeJson(new Rest.Call<Request, Response> {
                Url = Service,
                Method = Rest.Method.GET,
                Request = new Request {
                },
                LogLevel = LogLevel.Debug,
                ErrorLogLevel = LogLevel.Warn
            });
        }

    }
}
