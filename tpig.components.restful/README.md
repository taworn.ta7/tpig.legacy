**Tpig.Components.Restful/Restless**
====================================

A yet-another .NET RESTful library.


## Server

For server, see https://gitlab.com/taworn.ta7/tpig.examples.resttest.


## Requirements

For Tpig.Components.Restful, using Newtonsoft.Json
- .NET version 6.0

For Tpig.Components.Restless, using System.Text.Json
- .NET version 6.0


## Thank You

- Newtonsoft.Json, https://www.newtonsoft.com/json
- NLog, https://nlog-project.org
- gitignore, https://github.com/github/gitignore and https://gitignore.io


## NuGet

You can retrieve package at https://www.nuget.org/packages/Tpig.Components.Restful or https://www.nuget.org/packages/Tpig.Components.Restless.


## Last

Sorry, but I'm not good at English. T_T

Taworn Ta.

