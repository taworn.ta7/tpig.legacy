﻿using System;
using System.Windows.Input;

namespace Tpig.Internals {

    /// <summary>
    /// Relay the command.  Thank you for 
    /// https://stackoverflow.com/questions/22285866/why-relaycommand 
    /// and https://stackoverflow.com/users/632337/rohit-vats.
    /// </summary>
    internal class RelayCommand : ICommand {

        private readonly Action<object> execute;
        private readonly Predicate<object> canExecute;

        public RelayCommand(Action<object> execute) : this(execute, null) {
        }

        public RelayCommand(Action<object> execute, Predicate<object> canExecute) {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public void Execute(object parameter) {
            execute(parameter);
        }

        public bool CanExecute(object parameter) {
            return canExecute == null ? true : canExecute(parameter);
        }

        public event EventHandler CanExecuteChanged {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

    }

}
