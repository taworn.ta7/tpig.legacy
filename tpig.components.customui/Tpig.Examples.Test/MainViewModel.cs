﻿using System;
using System.ComponentModel;
using System.Windows.Input;

namespace Tpig.Examples.Test {
    public class MainViewModel : INotifyPropertyChanged {

        // Property Changed

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        // ----------------------------------------------------------------------

        // Change Locale

        private ICommand changeLocaleCommand = null;
        public ICommand ChangeLocaleCommand {
            get {
                if (changeLocaleCommand == null)
                    changeLocaleCommand = new Internals.RelayCommand(o => ChangeLocaleDelegate?.Invoke(o));
                return changeLocaleCommand;
            }
        }
        public Action<object> ChangeLocaleDelegate { get; set; } = null;

        // Message Command

        private ICommand messageCommand = null;
        public ICommand MessageCommand {
            get {
                if (messageCommand == null)
                    messageCommand = new Internals.RelayCommand(o => MessageDelegate?.Invoke(o));
                return messageCommand;
            }
        }
        public Action<object> MessageDelegate { get; set; } = null;

        // Wait Command

        private ICommand waitCommand = null;
        public ICommand WaitCommand {
            get {
                if (waitCommand == null)
                    waitCommand = new Internals.RelayCommand(o => WaitDelegate?.Invoke(o));
                return waitCommand;
            }
        }
        public Action<object> WaitDelegate { get; set; } = null;

    }
}
