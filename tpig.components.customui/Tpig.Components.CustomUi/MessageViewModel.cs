﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media;

namespace Tpig.Components.CustomUi {
    public class MessageViewModel : INotifyPropertyChanged {

        // Caption

        public string Caption { get; set; } = null;
        public bool ShowCaption { get => Caption != null; }

        // ----------------------------------------------------------------------

        // Message

        public string Message { get; set; } = null;

        // ----------------------------------------------------------------------

        // Background Color

        public Color? BackgroundColor { get; set; } = null;

        // ----------------------------------------------------------------------

        // Button Groups

        public enum Group {
            Close = 0,
            OkCancel,
            YesNo,
            RetryCancel
        }

        public Group Buttons { get; set; } = Group.Close;

        public bool GroupClose { get => Buttons == Group.Close; }
        public bool GroupOkCancel { get => Buttons == Group.OkCancel; }
        public bool GroupYesNo { get => Buttons == Group.YesNo; }
        public bool GroupRetryCancel { get => Buttons == Group.RetryCancel; }

        // ----------------------------------------------------------------------

        // Adorn Image

        public enum Adorn {
            None = 0,
            Info,
            Warn
        }

        public Adorn Decorate { get; set; } = Adorn.None;

        public bool AdornInfo { get => Decorate == Adorn.Info; }
        public bool AdornWarn { get => Decorate == Adorn.Warn; }

        // ----------------------------------------------------------------------

        // Auto Closing Timer

        public class AutoCloseTimer {
            // resource identifier loaded by LoadText()
            // it must be one {0} to format seconds
            public string Formatter { get; set; }

            // timeout
            public TimeSpan TimeOut { get; set; }

            // result in case timeout
            public Id Result { get; set; } = Id.Close;
        }

        public AutoCloseTimer AutoClose = null;

        // ----------------------------------------------------------------------

        // Result

        public enum Id {
            Close = 0,
            Ok,
            Cancel,
            Yes,
            No,
            Retry
        }

        public Id IdClose { get => Id.Close; }
        public Id IdOk { get => Id.Ok; }
        public Id IdCancel { get => Id.Cancel; }
        public Id IdYes { get => Id.Yes; }
        public Id IdNo { get => Id.No; }
        public Id IdRetry { get => Id.Retry; }

        public Id Result { get; set; } = Id.Close;

        private ICommand idCommand = null;
        public ICommand IdCommand {
            get {
                if (idCommand == null)
                    idCommand = new Internals.RelayCommand(o => {
                        if (o is Id)
                            IdDelegate?.Invoke((Id)o);
                    });
                return idCommand;
            }
        }
        public Action<Id> IdDelegate { get; set; } = null;

        // ----------------------------------------------------------------------

        // Property Changed

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        // ----------------------------------------------------------------------

        // Constructor

        public MessageViewModel() {
        }

        // ----------------------------------------------------------------------

        // Constructor with Packaged Dialog Set

        public enum DialogSet {
            NormalSet = 0,
            OkSet,
            OkCancelSet,
            YesNoSet,
            RetrySet,
            PassSet,
            InfoSet,
            CautionSet,
            WarnSet
        }

        public MessageViewModel(DialogSet set) {
            BackgroundColor = null;
            Buttons = Group.Close;
            Decorate = Adorn.Info;
            switch (set) {
                default:
                case DialogSet.NormalSet:
                    break;

                case DialogSet.OkCancelSet:
                    Buttons = Group.OkCancel;
                    break;
                case DialogSet.YesNoSet:
                    Buttons = Group.YesNo;
                    break;
                case DialogSet.RetrySet:
                    Buttons = Group.RetryCancel;
                    Decorate = Adorn.Warn;
                    break;

                case DialogSet.PassSet:
                    BackgroundColor = Color.FromArgb(0x80, 0, 0x80, 0);
                    break;
                case DialogSet.InfoSet:
                    BackgroundColor = Color.FromArgb(0x80, 0, 0x40, 0x80);
                    break;
                case DialogSet.CautionSet:
                    Decorate = Adorn.Warn;
                    BackgroundColor = Color.FromArgb(0x80, 0x80, 0x80, 0);
                    break;
                case DialogSet.WarnSet:
                    Decorate = Adorn.Warn;
                    BackgroundColor = Color.FromArgb(0x80, 0x80, 0, 0);
                    break;
            }
        }

    }
}
