**Tpig.Components.CustomUi**
============================

.NET WPF custom-made UI components.


## Requirements

- .NET version 6.0

- Tpig.Components.Localization, load from NuGet or https://gitlab.com/taworn.ta7/tpig.components.localization


## Thank You

- Crystal Clear icons, https://commons.wikimedia.org/wiki/Crystal_Clear
- Loading image, don't know where I get it, sorry T_T
- NLog, https://nlog-project.org
- gitignore, https://github.com/github/gitignore and https://gitignore.io


## NuGet

You can retrieve package at https://www.nuget.org/packages/Tpig.Components.CustomUi.


## Last

Sorry, but I'm not good at English. T_T

Taworn Ta.

