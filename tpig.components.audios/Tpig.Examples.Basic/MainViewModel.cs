﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Input;

namespace Tpig.Examples.Basic {
    public class MainViewModel : INotifyPropertyChanged {

        // Property Changed

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        // ----------------------------------------------------------------------

        // Open Audio Command

        private ICommand openAudioCommand = null;
        public ICommand OpenAudioCommand {
            get {
                if (openAudioCommand == null)
                    openAudioCommand = new RelayCommand(o => OpenAudioDelegate?.Invoke(o));
                return openAudioCommand;
            }
        }
        public Action<object> OpenAudioDelegate { get; set; } = null;

        // Delete Item Command

        private ICommand deleteItemCommand = null;
        public ICommand DeleteItemCommand {
            get {
                if (deleteItemCommand == null)
                    deleteItemCommand = new RelayCommand(o => DeleteItemExecute(o));
                return deleteItemCommand;
            }
        }
        private void DeleteItemExecute(object o) {
            if (o is int) {
                var i = (int)o;
                var found = AudioList.Find((item) => item.Id == i);
                if (found != null) {
                    found.Audio.Dispose();
                    AudioList.Remove(found);
                    AudioListView.Refresh();
                }
            }
        }

        // ----------------------------------------------------------------------

        // Audio List

        public List<AudioItem> AudioList { get; set; } = new List<AudioItem>();
        public ICollectionView AudioListView { get; set; } = null;

        // ----------------------------------------------------------------------

        // Constructor

        public MainViewModel() {
            AudioListView = CollectionViewSource.GetDefaultView(AudioList);
        }

    }
}
