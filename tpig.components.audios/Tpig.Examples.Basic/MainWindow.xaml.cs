﻿using System;
using System.IO;
using System.Windows;
using Tpig.Components.Audios;

namespace Tpig.Examples.Basic {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private MainViewModel viewModel = null;

        public MainWindow() {
            InitializeComponent();
            Loaded += (sender, e) => {
                viewModel = new MainViewModel();
                viewModel.OpenAudioDelegate += OpenAudioDelegate;
                AudioListView.ItemsSource = viewModel.AudioList;
                DataContext = viewModel;
            };
            Unloaded += (sender, e) => {
                DataContext = null;
                AudioListView.ItemsSource = null;
                viewModel.OpenAudioDelegate -= OpenAudioDelegate;
                viewModel = null;
            };
        }

        private void OpenAudioDelegate(object o) {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.DefaultExt = ".mp3";
            dialog.CheckFileExists = true;
            dialog.CheckPathExists = true;
            dialog.FileName = "";
            dialog.Filter = "Audio Files (*.mp3;*.aiff;*.aac;*.wma;*.wav)|*.mp3;*.aiff;*.aac;*.wma;*.wav|All Files (*.*)|*.*";
            var result = dialog.ShowDialog();
            if (result == true) {
                try {
                    var audio = new AudioFile(dialog.FileName);
                    var fileName = Path.GetFileName(dialog.FileName);
                    var isRepeat = IsRepeatCheck.IsChecked.Value;
                    var name = string.Format("[{0}] {1}", isRepeat ? "Repeat" : "Once", fileName);
                    var item = new AudioItem(name, audio);
                    viewModel.AudioList.Add(item);
                    viewModel.AudioListView.Refresh();
                    audio.Stopped += AudioStopped;
                    audio.UserData = item;
                    audio.Play(isRepeat);
                }
                catch (Exception) {
                    var message = string.Format("Cannot open file: {0}", dialog.FileName);
                    var caption = "Error";
                    MessageBox.Show(this, message, caption, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void AudioStopped(object sender, StoppedEventArgs e) {
            if (sender is IAudioFile) {
                var audio = sender as IAudioFile;
                if (!audio.IsRepeat) {
                    // delete ended audio
                    if (e.Data is AudioItem) {
                        var item = e.Data as AudioItem;
                        item.Audio.Dispose();
                        viewModel.AudioList.Remove(item);
                        viewModel.AudioListView.Refresh();
                    }
                }
            }
        }

    }
}
