﻿using Tpig.Components.Audios;

namespace Tpig.Examples.Basic {
    public class AudioItem {

        /// <summary>
        /// Identifier for this audio item, running number.
        /// </summary>
        public int Id { get; private set; } = 0;

        /// <summary>
        /// Audio filename.
        /// </summary>
        public string Name { get; private set; } = null;

        /// <summary>
        /// An audio file interface.
        /// </summary>
        public IAudioFile Audio { get; private set; } = null;

        /// <summary>
        /// Constructor.
        /// </summary>
        public AudioItem(string name, IAudioFile audio) {
            Id = runningId++;
            Name = name;
            Audio = audio;
        }

        // ----------------------------------------------------------------------

        private static int runningId = 0;

    }
}
