﻿using System;
using NAudio.Wave;

namespace Tpig.Components.Audios {
    /// <summary>
    /// Play an audio file and handle exceptions.
    /// </summary>
    public class ProxyAudioFile : IAudioFile {

        /// <summary>
        /// Inner audio file, or it is null if file is not exists.
        /// </summary>
        public AudioFile Inner {
            get;
            private set;
        } = null;

        // ----------------------------------------------------------------------

        private bool disposedValue = false;
        protected virtual void Dispose(bool disposing) {
            if (!disposedValue) {
                if (disposing) {
                    if (Inner != null)
                        Inner.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ProxyAudioFile() {
            Dispose(false);
        }

        public ProxyAudioFile(string fileName) {
            try {
                Inner = new AudioFile(fileName);
            }
            catch (Exception) {
                Inner = null;
            }
        }

        // ----------------------------------------------------------------------

        public void Play(bool isRepeated = false) {
            if (Inner != null) {
                try {
                    Inner.Play(isRepeated);
                }
                catch (Exception) {
                }
            }
        }

        public void Pause() {
            if (Inner != null) {
                try {
                    Inner.Pause();
                }
                catch (Exception) {
                }
            }
        }

        public void Resume() {
            if (Inner != null) {
                try {
                    Inner.Resume();
                }
                catch (Exception) {
                }
            }
        }

        public void Stop() {
            if (Inner != null) {
                try {
                    Inner.Stop();
                }
                catch (Exception) {
                }
            }
        }

        // ----------------------------------------------------------------------

        public AudioFileReader Reader { get => Inner?.Reader; }
        public WaveOutEvent Device { get => Inner?.Device; }

        public float Volume {
            get => Inner != null ? Inner.Volume : 0;
            set { if (Inner != null) Inner.Volume = value; }
        }

        public bool IsPlaying { get => Inner != null ? Inner.IsPlaying : false; }
        public bool IsRepeat { get => Inner != null ? Inner.IsRepeat : false; }

        public string FileName { get => Inner != null ? Inner.FileName : null; }

        public event EventHandler<StoppedEventArgs> Stopped {
            add {
                if (Inner != null)
                    Inner.Stopped += value;
            }
            remove {
                if (Inner != null)
                    Inner.Stopped -= value;
            }
        }

    }
}
