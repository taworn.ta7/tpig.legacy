﻿using System;

namespace Tpig.Components.Audios {

    /// <summary>
    /// Play an audio file.
    /// </summary>
    public interface IAudioFile : IDisposable {

        /// <summary>
        /// Play an audio.
        /// </summary>
        /// <param name="isRepeat">boolean to specify this playing is single(false) or repeat(true)</param>
        void Play(bool isRepeat = false);

        /// <summary>
        /// Pause an audio.
        /// </summary>
        void Pause();

        /// <summary>
        /// Resume a paused audio.
        /// </summary>
        void Resume();

        /// <summary>
        /// Stop an audio.
        /// </summary>
        void Stop();

        /// <summary>
        /// Volume, scale 0..1.
        /// </summary>
        float Volume { get; set; }

        /// <summary>
        /// Is playing?
        /// </summary>
        bool IsPlaying { get; }

        /// <summary>
        /// Is repeat after playback is end?
        /// </summary>
        bool IsRepeat { get; }

        /// <summary>
        /// Current filename of an andio.
        /// </summary>
        string FileName { get; }

        /// <summary>
        /// Event when the playback is end.
        /// </summary>
        event EventHandler<StoppedEventArgs> Stopped;

    }

    /// <summary>
    /// Data to pass in Stopped event handler.
    /// </summary>
    public class StoppedEventArgs : EventArgs {
        public object Data { get; set; } = null;
    }

}
