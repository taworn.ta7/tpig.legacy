﻿using System;
using NAudio.Wave;

namespace Tpig.Components.Audios {
    /// <summary>
    /// Play an audio file.
    /// </summary>
    public class AudioFile : IAudioFile {

        // internal objects
        public AudioFileReader Reader { get; private set; } = null;
        public WaveOutEvent Device { get; private set; } = null;

        // ----------------------------------------------------------------------

        // Disposing and Construction

        private bool disposedValue = false;
        protected virtual void Dispose(bool disposing) {
            if (!disposedValue) {
                if (disposing) {
                    playing = false;
                    Device.Stop();
                    Device.PlaybackStopped -= PlaybackStopped;
                    Device.Dispose();
                    Reader.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~AudioFile() {
            Dispose(false);
        }

        public AudioFile(string fileName) {
            FileName = fileName;
            Reader = new AudioFileReader(FileName);
            Device = new WaveOutEvent();
            Device.Init(Reader);
            Device.PlaybackStopped += PlaybackStopped;
        }

        // ----------------------------------------------------------------------

        // Play, Pause, Resume and Stop

        private bool playing = false;

        public void Play(bool isRepeat = false) {
            Reader.Position = 0;
            Device.Play();
            playing = true;
            IsRepeat = isRepeat;
        }

        public void Pause() {
            playing = false;
            Device.Stop();
        }

        public void Resume() {
            Device.Play();
            playing = true;
        }

        public void Stop() {
            playing = false;
            Device.Stop();
            Reader.Position = 0;
        }

        // ----------------------------------------------------------------------

        public float Volume {
            get => Device.Volume;
            set => Device.Volume = value;
        }

        public bool IsPlaying {
            get => Device.PlaybackState == PlaybackState.Playing;
        }

        public bool IsRepeat {
            get;
            private set;
        }

        public string FileName {
            get;
            private set;
        }

        public event EventHandler<StoppedEventArgs> Stopped;
        public object UserData { get; set; } = null;

        // ----------------------------------------------------------------------

        private void PlaybackStopped(object sender, NAudio.Wave.StoppedEventArgs e) {
            Stopped?.Invoke(this, new StoppedEventArgs() {
                Data = UserData
            });
            if (IsRepeat && playing) {
                Reader.Position = 0;
                Device.Play();
            }
        }

    }
}
