**Tpig.Components.Audios**
==========================

A facade audio (backend NAudio) playing.


## How to Use

Create

    var audio = new AudioFile(dialog.FileName);

    -- or --

    var audio = new ProxyAudioFile(dialog.FileName);  // for tolerate file not found error

Play

    audio.Play(isRepeat);

Destroy (optional)

    audio.Dispose();


## Requirements

- .NET version 6.0


## Thank You

- NAudio, https://github.com/naudio/NAudio
- gitignore, https://github.com/github/gitignore and https://gitignore.io


## NuGet

You can retrieve package at https://www.nuget.org/packages/Tpig.Components.Audios.


## Last

Sorry, but I'm not good at English. T_T

Taworn Ta.

