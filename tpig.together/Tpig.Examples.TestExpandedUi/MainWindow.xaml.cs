﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using NLog;
using Tpig.Boosters.ExpandedUi;
using Tpig.Components.CustomUi;
using Tpig.Components.Localization;

namespace Tpig.Examples.TestExpandedUi {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private MainViewModel viewModel = null;

        private Timer timer = null;

        public MainWindow() {
            InitializeComponent();
            LocaleManager.Instance.Current = "en";
            Localize.Load(this);

            Loaded += (sender, e) => {
                viewModel = new MainViewModel();
                viewModel.ChangeLocaleDelegate += ChangeLocaleDelegate;
                viewModel.MessageDelegate += MessageDelegate;
                viewModel.WaitDelegate += WaitDelegate;
                DataContext = viewModel;

                ChangeLocaleEnButton.IsChecked = ChangeLocaleEnButton.CommandParameter.ToString().Equals(LocaleManager.Instance.Current);
                ChangeLocaleThButton.IsChecked = ChangeLocaleThButton.CommandParameter.ToString().Equals(LocaleManager.Instance.Current);
            };

            Unloaded += (sender, e) => {
                if (timer != null) {
                    timer.Change(Timeout.Infinite, Timeout.Infinite);
                    timer.Dispose();
                    timer = null;
                }

                DataContext = null;
                viewModel.WaitDelegate -= WaitDelegate;
                viewModel.MessageDelegate -= MessageDelegate;
                viewModel.ChangeLocaleDelegate -= ChangeLocaleDelegate;
            };
        }

        private void ChangeLocaleDelegate(object o) {
            var locale = o as string;
            LocaleManager.Instance.Current = locale;
            logger.Info("change locale to: {0}", locale);
        }

        private void MessageDelegate(object o) {
            if (o.Equals("Question")) {
                var vm = new MessageViewModel {
                    //Caption = this.Text("Message_Question"),
                    Message = this.Text("Message_Question"),
                    Buttons = MessageViewModel.Group.RetryCancel,
                    Decorate = MessageViewModel.Adorn.Warn,
                    AutoClose = new MessageViewModel.AutoCloseTimer {
                        Formatter = this.Text("Message_TimeLeft"),
                        TimeOut = TimeSpan.FromMilliseconds(4999),
                        Result = MessageViewModel.Id.No
                    }
                };
                var result = ExpandedMessageWindow.Execute(this, vm);
                logger.Info("question, result: {0}", result);
            }

            else if (o.Equals("Info")) {
                var caption = this.Text("Message_Info");
                var message = this.Text("Message_Info");
                var result = ExpandedMessageWindow.Execute(this, MessageViewModel.DialogSet.InfoSet, message, caption);
                logger.Info("information, result: {0}", result);
            }

            else if (o.Equals("Warn")) {
                var caption = this.Text("Message_Warn");
                var message = this.Text("Message_Warn");
                var result = ExpandedMessageWindow.Execute(this, MessageViewModel.DialogSet.WarnSet, message, caption);
                logger.Info("warning, result: {0}", result);
            }
        }

        private void WaitDelegate(object o) {
            if (o.Equals("Background")) {
                var message = this.Text("Wait_Text");
                var result = ExpandedWaitWindow.Execute(this, (_0) => {
                    Thread.Sleep(1999);
                    return true;
                }, message, Color.FromArgb(0xcc, 0x00, 0x00, 0x00));
                logger.Info("waiting, return: {0}", result);
            }
            else if (o.Equals("Foreground")) {
                var message = this.Text("Wait_Text");
                ExpandedWaitControl.Begin(message, Color.FromArgb(0x88, 0x44, 0x99, 0xcc));
                timer = new Timer((_0) => Dispatcher.Invoke(() => {
                    ExpandedWaitControl.End();
                    timer.Dispose();
                    timer = null;
                }), null, 1999, 0);
            }
        }

    }
}
