﻿using System.Windows;
using System.Windows.Controls;
using NLog;
using Tpig.Components.Localization;
using Tpig.Components.Navigators;
using Tpig.Examples.Ui.Popups;

namespace Tpig.Examples.Ui {
    /// <summary>
    /// Interaction logic for TwoControl.xaml
    /// </summary>
    public partial class TwoControl : UserControl, IWindowExtensions {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private string navigateKey = null;
        private string name = null;

        public TwoControl(string navigateKey) {
            InitializeComponent();
            Localize.Load(this);
            this.navigateKey = navigateKey;
        }

        // ----------------------------------------------------------------------

        public void Enter(string name) {
            logger.Trace("{0}: enter", name);
            this.name = name;
        }

        public void Leave(string name) {
            logger.Trace("{0}: leave", name);
            this.name = null;
        }

        // ----------------------------------------------------------------------

        private void Back(object sender, RoutedEventArgs e) {
            var parameter = (sender as Button)?.CommandParameter as string;
            CommonControl.Back(navigateKey, name, parameter);
        }

        private void Next(object sender, RoutedEventArgs e) {
            var parameter = (sender as Button)?.CommandParameter as string;
            CommonControl.Next(navigateKey, name, parameter);
        }

        private void ChangeLocale(object sender, RoutedEventArgs e) {
            var parameter = (sender as Button)?.CommandParameter as string;
            CommonControl.ChangeLocale(name, parameter);
        }

        private void Exit(object sender, RoutedEventArgs e) {
            CommonControl.Exit(name, this);
        }

        // ----------------------------------------------------------------------

        private void Center(object sender, RoutedEventArgs e) {
            logger.Trace("{0}: center", name);
            var popupHost = Application.Current.Properties[navigateKey + "/Popup"] as IPopupHost;
            SamplePopupControl.Open(popupHost, "Text 1", "Text 2", (result, text1, text2) => {
                logger.Trace("{0}: result={1}", name, result);
                logger.Trace("{0}: text1={1}, text2={2}", name, text1, text2);
            });
        }

    }
}
