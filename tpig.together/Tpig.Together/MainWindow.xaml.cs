﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Microsoft.Extensions.Configuration;
using NLog;
using Tpig.Boosters;
using Tpig.Components.AppConfigs;
using Tpig.Components.Localization;
using Tpig.Components.Navigators;
using Tpig.Helpers;
using Tpig.Together.Applets;

namespace Tpig.Together {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public MainWindow() {
            InitializeComponent();
            LocaleManager.Instance.Current = "en";
            Localize.Load(this);

            SetupAppConfigs();
            SetupLogs();
            SetupUi(Container, Popup);

            Loaded += (sender, e) => {
                SetupWindow(this);

                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)(() => {
                    SoftKeyboardControl.AddThaiLanguage();
                }));
            };

            Unloaded += (sender, e) => {
            };

            Closed += (sender, e) => {
            };
        }

        // ----------------------------------------------------------------------

        /// <summary>
        /// Setup application's configurations.
        /// </summary>
        private static void SetupAppConfigs() {
            // setup new configurations
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables()
                .AddJsonFile(AppPath.Path("appConfigs.json"), optional: false, reloadOnChange: true)
                .AddJsonFile(Environment.ExpandEnvironmentVariables(@"%LOCALAPPDATA%\Tpig.Together\localConfigs.json"), optional: true, reloadOnChange: true)
                .Build();
            MergeConfig.Instance.Add(new ConfigurationBridge(config));

            // add ini file
            var iniFile = MergeConfig.Instance.CombineReadExpandedString("IniFile");
            if (!string.IsNullOrWhiteSpace(iniFile)) {
                MergeConfig.Instance.UserConfig = new IniFile(iniFile);
            }
        }

        /// <summary>
        /// Setup application's loggings.
        /// </summary>
        private static void SetupLogs() {
            // delete old log files
            var logPath = MergeConfig.Instance.CombineReadExpandedString("LogPath");
            if (!string.IsNullOrWhiteSpace(logPath)) {
                var oldDays = MergeConfig.Instance.CombineReadInt("KeepLogAsDays", 1);
                Booster.LogDeleter(oldDays, logPath, @"^([a-zA-Z0-9]+-)?([\d]{4})([\d]{2})([\d]{2}).*\.log$",
                    yearIndex: 2, monthIndex: 3, dayIndex: 4);
            }
        }

        /// <summary>
        /// Setup UI elements.
        /// </summary>
        private static void SetupUi(Panel container, Panel popup) {
            // add UserControl(s) to ControlMap
            var controlMap = new ControlMap();
            controlMap.Add("SampleControl", () => new SampleControl());

            // create NavigateMap based on created ControlMap
            var navigateMap = new NavigateMap(container, controlMap, "SampleControl");
            // create popup helper
            var popupHost = new PopupHost(container, popup);

            // copy navigate map and popup helper to application's properties
            var props = Application.Current.Properties;
            props["NavigateKey"] = navigateMap;
            props["NavigateKey/Popup"] = popupHost;
        }

        /// <summary>
        /// Setup main window.
        /// </summary>
        private static void SetupWindow(Window window) {
            var monitors = Win32.GetAllMonitors();
#if DEBUG
            var monitor = Booster.ChooseMonitorToDisplay(window, monitors, Booster.ChooseMonitorType.BiggestAlign, true);
            Booster.ResizeAndMoveWindow(window,
                desireSize: new Size(1440, 900),
                monitorRect: monitor.WorkArea,
                shrinkSizeIfNeed: true, expandSizeIfNeed: true,
                Booster.WindowPositionType.CenterCenter);
#else
            var monitor = Booster.ChooseMonitorToDisplay(window, monitors, Booster.ChooseMonitorType.Monitor0, true);
            Booster.MoveWindowToChosenMonitor(window,
                monitorRect: monitor.WorkArea,
                expandToFit: true);
#endif
        }

    }
}
