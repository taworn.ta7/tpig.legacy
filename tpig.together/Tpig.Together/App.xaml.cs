﻿using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Windows;
using NLog;
using Tpig.Helpers;

namespace Tpig.Together {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static Mutex mutex = new Mutex(true, AppPath.Instance.OnlyName);

        private static void ExceptionHandler(object sender, UnhandledExceptionEventArgs args) {
            var ex = (Exception)args.ExceptionObject;
            logger.Fatal(ex);
            logger.Fatal("PROGRAM CRASH T_T");
            logger.Info("");
            logger.Info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            logger.Info("");
#if !DEBUG
            Environment.Exit(-1);
#endif
        }

        App() {
            // final chance to handle error
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(ExceptionHandler);

            // single instance only
            if (!mutex.WaitOne(TimeSpan.Zero, true)) {
                logger.Info("single instance only!");
                Shutdown();
                return;
            }
            mutex.ReleaseMutex();

            // support HTTPS
            ServicePointManager.ServerCertificateValidationCallback += delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
                // always accept
                return true;
            };

            // logging
            logger.Info("");
            logger.Info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            logger.Info("$ BEGIN APPS                                                         $");
            logger.Info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            logger.Info("");
            logger.Info("app path: {0}", AppPath.Instance.FullName);
            Exit += (sender, e) => {
                logger.Info("");
                logger.Info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                logger.Info("");
            };
        }

    }
}
