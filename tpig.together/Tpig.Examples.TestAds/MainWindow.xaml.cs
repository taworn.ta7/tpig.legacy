﻿using System;
using System.Windows;
using NLog;
using Tpig.Boosters.Ads;
using Tpig.Helpers;

namespace Tpig.Examples.TestAds {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public MainWindow() {
            InitializeComponent();

            Loaded += (sender, e) => {
                SetupAds();
            };
        }

        public void SetupAds() {
            //AdViewer.LoadBackground(AppPath.Path(@"Ads\back.png"));
            var adPath = AppPath.Path("Ads");
            var cachePath = AppPath.Path("Cache");
            var rotator = AdRotator.Instance.Setup(AdViewer).Setup(adPath, cachePath);
            rotator.AddToQueue("t1.jpg", "http://localhost:55555/ads/t1.jpg", new DateTime(2020, 1, 21, 15, 23, 0), new DateTime(2020, 3, 30, 15, 30, 0));
            rotator.AddToQueue("t2.jpg", "http://localhost:55555/ads/t2.jpg", null, new DateTime(2020, 3, 17));
            rotator.AddToQueue("t3.jpg", "http://localhost:55555/ads/t3.jpg", new DateTime(2020, 2, 17), null, TimeSpan.FromSeconds(15));
            rotator.AddToQueue("t4.jpg", "http://localhost:55555/ads/t4.jpg", null, null);
            rotator.Log(true);
        }

        // ----------------------------------------------------------------------

        private void StartClick(object sender, RoutedEventArgs e) {
            AdRotator.Instance.Start();
        }

        private void StopClick(object sender, RoutedEventArgs e) {
            AdRotator.Instance.Stop();
        }

        private void ResetupClick(object sender, RoutedEventArgs e) {
            try {
                AdRotator.Instance.Reset();

                var adPath = AppPath.Path("Ads");
                var cachePath = AppPath.Path("Cache");
                AdRotator.Instance.Setup(AdViewer).Setup(adPath, cachePath);
            }
            catch (InvalidOperationException ex) {
                MessageBox.Show(this, ex.Message, "CAUTION!");
            }
        }

    }
}
