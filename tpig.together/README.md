**Tpig.Together**
=================

This is a sample program which I use in template.


## Tpig.Boosters

It's a library that I wrote when I used it in company.  Here:

- ComputerStatus: cpu ram and harddisk monitoring
- LogDeleter: delete logs, log files must have yyyy MM dd pattern
- SceneTimer: set timeout, navigators extension, see Tpig.Examples.TestSceneTimer
- Ads: displaying advertising, see Tpig.Examples.TestAds
- ExpandedUi: an UI style, see Tpig.Examples.TestExpandedUi

You can use it as you see fit.


## Requirements

- .NET version 6.0


## Thank You

- NLog, https://nlog-project.org
- Loading image, https://loading.io
- gitignore, https://github.com/github/gitignore and https://gitignore.io


## NuGet

You can retrieve package at https://www.nuget.org/packages/Tpig.Boosters. 


## Last

Sorry, but I'm not good at English. T_T

Taworn Ta.

