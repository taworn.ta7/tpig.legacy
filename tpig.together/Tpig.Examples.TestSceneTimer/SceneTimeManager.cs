﻿using System;
using Tpig.Boosters.SceneTimers;

namespace Tpig.Examples.TestSceneTimer {

    class SceneTiming : BasicSceneTiming {
        public string Name { get; private set; }
        public SceneTiming(string name) {
            Name = name;
            TimeOut = TimeSpan.FromMilliseconds(7000);
            TimeWarn = TimeSpan.FromMilliseconds(5000);
            Destination = "OneControl";
        }
        public override string ToString() =>
            string.Format("{0}: timeout={1},warn={2},dest={3}", Name, TimeOut, TimeWarn, Destination);
    }

    class SceneTimeManager : ISceneTimeManager {
        public ISceneTiming Get(string name) {
            // every 'name' return same timeout, except OneControl
            if (name.Equals("OneControl"))
                return null;
            else
                return new SceneTiming(name);
        }
    }

}
