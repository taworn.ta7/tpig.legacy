﻿using System;
using System.Windows;
using System.Windows.Controls;
using NLog;
using Tpig.Components.Localization;
using Tpig.Components.Navigators;
using Tpig.Examples.Ui;
using Tpig.Boosters;
using Tpig.Boosters.SceneTimers;

namespace Tpig.Examples.TestSceneTimer {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDrawingTimeWarn {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public MainWindow() {
            InitializeComponent();
            LocaleManager.Instance.Current = "en";
            Localize.Load(this);

            SetupUi("NavigateMap", Container, Popup);
        }

        private void SetupUi(string navigateKey, Panel container, Panel popup) {
            var controlMap = new ControlMap();
            controlMap.Add("OneControl", () => new OneControl(navigateKey));
            controlMap.Add("TwoControl", () => new TwoControl(navigateKey));
            controlMap.Add("ThreeControl", () => new ThreeControl(navigateKey));
            controlMap.Add("FourControl", () => new FourControl(navigateKey), "TwoControl");

            var navigateMap = new NavigateMap(container, controlMap, "OneControl");
            Application.Current.Properties[navigateKey] = navigateMap;
            var popupHost = new PopupHost(container, popup);
            Application.Current.Properties[navigateKey + "/Popup"] = popupHost;

            var sceneTimeManager = new SceneTimeManager();
            var sceneTimer = new SceneTimer()
                .Setup(navigateMap)
                .Setup(popupHost)
                .Setup(sceneTimeManager)
                .Setup(this);
            Application.Current.Properties[navigateKey + "/SceneTimer"] = sceneTimer;
        }

        // ----------------------------------------------------------------------

        public void Off() {
            TimeWarnText.Text = null;
        }

        public void On(TimeSpan t) {
            var format = TimeWarnText.Tag as string;
            TimeWarnText.Text = string.Format(format, (int)t.TotalSeconds);
        }

    }
}
