﻿using System;
using System.Windows;
using System.Windows.Controls;
using NLog;
using Tpig.Components.Localization;
using Tpig.Components.Navigators;

namespace Tpig.Together.Applets.Popups {
    /// <summary>
    /// Interaction logic for SamplePopupControl.xaml
    /// </summary>
    public partial class SamplePopupControl : UserControl {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private IPopupHost popupHost = null;

        private SamplePopupControl() {
            InitializeComponent();
            Localize.Load(this);
        }

        // ----------------------------------------------------------------------

        public Action<bool, string, string> Result { get; set; } = null;

        private void Ok(object sender, RoutedEventArgs e) {
            popupHost.Detach();
            Result?.Invoke(true, Text1.Text, Text2.Password);
            Result = null;
        }

        private void Cancel(object sender, RoutedEventArgs e) {
            popupHost.Detach();
            Result?.Invoke(false, null, null);
            Result = null;
        }

        // ----------------------------------------------------------------------

        public static void Open(IPopupHost host, string text1, string text2, Action<bool, string, string> result) {
            var control = new SamplePopupControl();
            control.popupHost = host;
            control.popupHost.Attach(control);
            control.Text1.Text = text1;
            control.Text2.Password = text2;
            control.Result = result;
        }

    }
}
