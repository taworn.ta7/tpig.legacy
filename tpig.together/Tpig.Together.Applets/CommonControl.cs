﻿using System.Windows;
using System.Windows.Controls;
using NLog;
using Tpig.Components.Localization;
using Tpig.Components.Navigators;

namespace Tpig.Together.Applets {
    internal static class CommonControl {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void Back(string name, string parameter) {
            logger.Trace("{0}: back, parameter={1}", name, parameter);
            var navigateMap = Application.Current.Properties["NavigateKey"] as NavigateMap;
            navigateMap?.Back(parameter);
        }

        public static void Next(string name, string parameter) {
            logger.Trace("{0}: next, parameter={1}", name, parameter);
            var navigateMap = Application.Current.Properties["NavigateKey"] as NavigateMap;
            navigateMap?.Next(parameter);
        }

        public static void ChangeLocale(string name, string parameter) {
            logger.Trace("{0}: change locale to {1}", name, parameter);
            LocaleManager.Instance.Current = parameter;
        }

        public static void Exit(string name, UserControl control) {
            logger.Trace("{0}: exit", name);
            Window.GetWindow(control).Close();
        }

    }
}
