﻿using System;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Runtime.InteropServices;
using NLog;

namespace Tpig.Boosters {
    public class ComputerStatus : IDisposable {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        // processor name
        public string ProcessorName { get; private set; }

        // counter CPU, as %
        public float CounterCpu { get => counterCpu != null ? counterCpu.NextValue() : 0; }

        // counter RAM, as MB
        public float CounterRam { get => counterRam != null ? counterRam.NextValue() : 0; }

        // physical and virtual memory, as MB
        public float MemoryPhysical { get; private set; } = 0;
        public float MemoryVirtual { get; private set; } = 0;

        // harddisk size and free, as GB, drive C:
        public float HarddiskSize0 { get; private set; } = 0;
        public float HarddiskFree0 { get => GetHarddiskFree(0); }

        // harddisk size and free, as GB, drive D:
        public float HarddiskSize1 { get; private set; } = 0;
        public float HarddiskFree1 { get => GetHarddiskFree(1); }

        // ----------------------------------------------------------------------

        // CPU & RAM counters
        private PerformanceCounter counterCpu;
        private PerformanceCounter counterRam;

        // memory status
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public class MEMORYSTATUSEX {
            public uint dwLength;
            public uint dwMemoryLoad;
            public ulong ullTotalPhys;
            public ulong ullAvailPhys;
            public ulong ullTotalPageFile;
            public ulong ullAvailPageFile;
            public ulong ullTotalVirtual;
            public ulong ullAvailVirtual;
            public ulong ullAvailExtendedVirtual;
            public MEMORYSTATUSEX() => dwLength = (uint)Marshal.SizeOf(typeof(MEMORYSTATUSEX));
        }
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern bool GlobalMemoryStatusEx([In, Out] MEMORYSTATUSEX lpBuffer);

        // ----------------------------------------------------------------------

        // constructor
        public ComputerStatus() {
            // processor
            using (var proc = new ManagementObjectSearcher("select * from Win32_Processor")) {
                foreach (var o in proc.Get()) {
                    ProcessorName = o["Name"].ToString();
                }
            }

            // initialize CPU counter
            try {
                counterCpu = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            }
            catch (Exception ex) {
                logger.Error(ex);
            }

            // initialize RAM counter
            try {
                counterRam = new PerformanceCounter("Memory", "Available MBytes");
            }
            catch (Exception ex) {
                logger.Error(ex);
            }

            // call GlobalMemoryStatusEx() to get memory status
            var memoryStatus = new MEMORYSTATUSEX();
            GlobalMemoryStatusEx(memoryStatus);
            MemoryPhysical = memoryStatus.ullTotalPhys / (1024f * 1024f);
            MemoryVirtual = memoryStatus.ullTotalPageFile / (1024f * 1024f);

            // get infomation on drive C:
            foreach (var drive in DriveInfo.GetDrives()) {
                if (drive.Name == "C:\\" && drive.IsReady)
                    HarddiskSize0 = drive.TotalSize / 1024f / 1024f / 1024f;
                if (drive.Name == "D:\\" && drive.IsReady)
                    HarddiskSize1 = drive.TotalSize / 1024f / 1024f / 1024f;
            }
        }

        // get harddisk free size, driveNumber 0=C, 1=D
        private float GetHarddiskFree(int driveNumber) {
            foreach (var drive in DriveInfo.GetDrives()) {
                if (drive.Name == "C:\\" && drive.IsReady && driveNumber == 0)
                    return drive.AvailableFreeSpace / 1024f / 1024f / 1024f;
                if (drive.Name == "D:\\" && drive.IsReady && driveNumber == 1)
                    return drive.AvailableFreeSpace / 1024f / 1024f / 1024f;
            }
            return 0;
        }

        private bool disposedValue = false;
        protected virtual void Dispose(bool disposing) {
            if (!disposedValue) {
                if (disposing) {
                    if (counterRam != null)
                        counterRam.Dispose();
                    if (counterCpu != null)
                        counterCpu.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // destructor
        ~ComputerStatus() {
            Dispose(false);
        }

    }
}
