﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using NLog;
using Tpig.Helpers.CommonUi;

namespace Tpig.Boosters.Ads {
    /// <summary>
    /// Interaction logic for AdViewerControl.xaml
    /// </summary>
    public partial class AdViewerControl : UserControl {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static List<string> ImageExtensionList = new List<string>() {
            ".png",
            ".jpg",
            ".jpeg"
        };

        private static List<string> MediaExtensionList = new List<string>() {
            ".mkv",
            ".mp4",
            ".m4v"
        };

        private DispatcherTimer timer = null;

        public TimeSpan ImageInterval { get; set; } = TimeSpan.FromSeconds(10);

        public Action End { get; set; } = null;

        public bool ShowBackground {
            get => !ShowImage && !ShowMedia;
            set {
                BackgroundImage.Visibility = Visibility.Visible;
                Image.Visibility = Visibility.Hidden;
                Media.Visibility = Visibility.Hidden;
            }
        }
        public bool ShowImage {
            get => Image.Visibility == Visibility.Visible;
            set {
                BackgroundImage.Visibility = Visibility.Hidden;
                Image.Visibility = Visibility.Visible;
                Media.Visibility = Visibility.Hidden;
            }
        }
        public bool ShowMedia {
            get => Image.Visibility == Visibility.Visible;
            set {
                BackgroundImage.Visibility = Visibility.Hidden;
                Image.Visibility = Visibility.Hidden;
                Media.Visibility = Visibility.Visible;
            }
        }

        public AdViewerControl() {
            InitializeComponent();

            timer = new DispatcherTimer {
                Interval = ImageInterval
            };
            timer.Tick += (sender, e) => {
                logger.Trace("end image");
                timer.IsEnabled = false;
                ShowBackground = true;
                End?.Invoke();
            };
            timer.IsEnabled = false;

            Loaded += (sender, e) => {
                ShowBackground = true;

                Media.MediaEnded += OnMediaEnded;
                Media.MediaFailed += OnMediaFailed;
            };

            Unloaded += (sender, e) => {
                Media.MediaFailed -= OnMediaFailed;
                Media.MediaEnded -= OnMediaEnded;
            };
        }

        // ----------------------------------------------------------------------

        public ImageSource BackSource {
            get => BackgroundImage.Source;
            set => BackgroundImage.Source = value;
        }

        public bool LoadBackground(string imageFileName) {
            try {
                var bitmap = Bitmap.FromFile(imageFileName) as Bitmap;
                BackgroundImage.Source = Helpful.BitmapToBitmapSource(bitmap);
                return true;
            }
            catch (Exception ex) {
                logger.Error(ex);
                return false;
            }
        }

        // ----------------------------------------------------------------------

        public bool Load(string fileName, TimeSpan timeStay) {
            ShowBackground = true;

            var ext = Path.GetExtension(fileName).Trim();
            var found = ImageExtensionList.Find(x => x.Equals(ext));
            if (found != null) {
                // load image
                try {
                    var bitmap = Bitmap.FromFile(fileName) as Bitmap;
                    var source = Helpful.BitmapToBitmapSource(bitmap);
                    Image.Source = source;
                    ImageInterval = timeStay;
                    ShowImage = true;

                    timer.Interval = ImageInterval;
                    timer.IsEnabled = true;
                    logger.Trace("{0}: load success", Path.GetFileName(fileName));
                    return true;
                }
                catch (Exception ex) {
                    logger.Error(ex);
                    return false;
                }
            }

            found = MediaExtensionList.Find(x => x.Equals(ext));
            if (found != null) {
                // load media
                try {
                    Media.Source = new Uri(fileName);
                    ShowMedia = true;
                    Media.Play();
                    logger.Trace("{0}: load success", Path.GetFileName(fileName));
                    return true;
                }
                catch (Exception ex) {
                    logger.Error(ex);
                    return false;
                }
            }

            // no support extensions
            logger.Warn("no support extensions ({0})", ext);
            return false;
        }

        public void Stop() {
            ShowBackground = true;
            timer.IsEnabled = false;
            Media.Stop();
        }

        // ----------------------------------------------------------------------

        private void OnMediaEnded(object sender, RoutedEventArgs args) {
            logger.Trace("end video");
            ShowBackground = true;
            End?.Invoke();
        }

        private void OnMediaFailed<ExceptionRoutedEventArgs>(object sender, ExceptionRoutedEventArgs args) {
            logger.Trace("end video error!");
            ShowBackground = true;
            End?.Invoke();
        }

    }
}
