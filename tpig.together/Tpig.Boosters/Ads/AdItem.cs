﻿using System;

namespace Tpig.Boosters.Ads {
    public class AdItem {
        /// <summary>
        /// Filename without folder.
        /// </summary>
        public string FileName { get; set; } = null;

        public string Url { get; set; } = null;

        public DateTime? Begin { get; set; } = null;

        public DateTime? End { get; set; } = null;

        public TimeSpan? TimeStay { get; set; } = null;

        public bool IsDownloaded { get; set; } = false;

        public bool IsOk {
            get {
                if (!IsDownloaded)
                    return false;

                var now = DateTime.Now;
                if (Begin != null && !(Begin <= now))
                    return false;
                if (End != null && !(now < End))
                    return false;

                return true;
            }
        }

        public object UserData { get; set; } = null;

        public AdItem() {
            UserData = Guid.NewGuid();
        }

        public override string ToString() =>
            string.Format("- {0}, downloaded={1}, [{2:yyyy-MM-dd HH:mm:ss}, {3:yyyy-MM-dd HH:mm:ss})", FileName, IsDownloaded, Begin, End);
    }
}
