﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Threading;
using NLog;
using Tpig.Components.Downloaders;

namespace Tpig.Boosters.Ads {
    /// <summary>
    /// Ad facade to bind downloader and viewer.
    /// </summary>
    public class AdRotator {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Singleton instance.
        /// </summary>
        public static AdRotator Instance {
            get {
                if (instance == null)
                    instance = new AdRotator();
                return instance;
            }
        }
        private static AdRotator instance = null;

        private AdRotator() {
        }

        // ----------------------------------------------------------------------

        // Setup AdViewerControl

        public AdViewerControl Viewer {
            get;
            private set;
        } = null;

        public AdRotator Setup(AdViewerControl viewer) {
            if (Viewer != null)
                throw new ArgumentException("Viewer already set!");

            Viewer = viewer;
            Viewer.End += ViewerEndEvent;
            return this;
        }

        private void ViewerEndEvent() {
            Viewer.Dispatcher.BeginInvoke(new Action(() => {
                if (timer != null)
                    PlayOrPause();
            }));
        }

        // ----------------------------------------------------------------------

        // Setup Ad Folder and Cache Folder

        private Downloader downloader = new Downloader();

        public string AdFolder {
            get;
            private set;
        } = null;

        public string CacheFolder {
            get;
            private set;
        } = null;

        public AdRotator Setup(string adFolder, string cacheFolder) {
            if (AdFolder != null)
                throw new ArgumentException("AdFolder already set!");
            if (CacheFolder != null)
                throw new ArgumentException("CacheFolder already set!");

            AdFolder = adFolder;
            CacheFolder = cacheFolder;

            Directory.CreateDirectory(AdFolder);
            Directory.CreateDirectory(CacheFolder);
            return this;
        }

        // ----------------------------------------------------------------------

        // Reset and re-setup

        public void Reset() {
            if (timer != null)
                throw new InvalidOperationException("AdRotator is running, please call Stop()!");
            if (Viewer != null) {
                Viewer.End -= ViewerEndEvent;
                Viewer = null;
            }
            AdFolder = null;
            CacheFolder = null;
        }

        // ----------------------------------------------------------------------

        // Manage Ad Queue

        private List<AdItem> adList = new List<AdItem>();

        public int AdCount {
            get => adList.Count;
        }

        /// <summary>
        /// Add new ad.
        /// </summary>
        public void AddToQueue(string fileName, string url, DateTime? begin, DateTime? end, TimeSpan? timeStay = null) {
            var item = new AdItem {
                FileName = fileName,
                Url = url,
                Begin = begin,
                End = end,
                TimeStay = timeStay
            };
            AddToQueue(item);
        }

        /// <summary>
        /// Add new ad.
        /// </summary>
        public void AddToQueue(AdItem item) {
            adList.Add(item);

            // check if file already loaded
            var fileName = Path.Combine(AdFolder, item.FileName);
            if (File.Exists(fileName)) {
                // ok, mark downloaded
                item.IsDownloaded = true;
            }
            else {
                // put to downloader queue
                var q = new Queue {
                    Url = item.Url,
                    FileName = Path.Combine(CacheFolder, item.FileName),  // download to cache path
                    Callback = QueueCallback,
                    Progress = QueueProgress,
                    UserData = item.UserData
                };
                downloader.AddToQueue(q);
            }
        }

        public void Clear() =>
            adList.Clear();

        // ----------------------------------------------------------------------

        // Start and Stop Running Ads

        public void Start() {
            if (Viewer == null)
                throw new InvalidOperationException("Viewer is not set!");
            if (AdFolder == null)
                throw new InvalidOperationException("AdFolder is not set!");
            if (CacheFolder == null)
                throw new InvalidOperationException("CacheFolder is not set!");

            if (timer == null) {
                timer = new DispatcherTimer(DispatcherPriority.Background);
                timer.Tick += Tick;
                timer.Interval = TimeSpan.FromMilliseconds(1000);
                timer.IsEnabled = true;
                downloader.Start();
                logger.Debug("{0}: start", GetType().Name);
            }
        }

        public void Stop() {
            if (timer != null) {
                logger.Debug("{0}: stop", GetType().Name);
                Viewer?.Stop();
                downloader.Stop();
                timer.IsEnabled = false;
                timer = null;
            }
        }

        // ----------------------------------------------------------------------

        // Ad Display

        public TimeSpan TimeStay { get; set; } = TimeSpan.FromSeconds(10);

        private DispatcherTimer timer = null;
        private int playIndex = 0;

        private void Tick(object sender, EventArgs e) =>
            PlayOrPause();

        private void PlayOrPause() {
            var index = FindNext();
            if (index >= 0) {
                // disable timer
                timer.IsEnabled = false;

                // play ad by index
                playIndex = index;
                Viewer.Dispatcher.BeginInvoke(new Action(() => {
                    var ad = adList[playIndex];
                    var fileName = Path.Combine(AdFolder, ad.FileName);
                    logger.Trace("{0}: play {1}, index={2}", GetType().Name, fileName, playIndex);
                    Viewer.Load(fileName, ad.TimeStay ?? TimeStay);
                    playIndex++;
                }));
            }
        }

        private int FindNext() {
            var count = adList.Count;
            if (playIndex >= count)
                playIndex = 0;

            for (var i = playIndex; i < count; i++) {
                if (adList[i].IsOk)
                    return i;
            }
            if (playIndex > 0) {
                for (var i = 0; i < playIndex; i++)
                    if (adList[i].IsOk)
                        return i;
            }

            return -1;
        }

        // ----------------------------------------------------------------------

        // Downloading

        private void QueueCallback(Queue q, DownloadedStatusType status) {
            logger.Trace("{0}: {1}, result={2}", Path.GetFileName(q.FileName), q.Url, status);

            if (status == DownloadedStatusType.Ok) {
                // success download, move file to ad folder
                var found = adList.Find(x => x.UserData == q.UserData);
                if (found != null) {
                    logger.Trace("{0}: move file to {1}", Path.GetFileName(q.FileName), AdFolder);
                    File.Move(q.FileName, Path.Combine(AdFolder, found.FileName));
                    found.IsDownloaded = true;
                }
            }
            else if (status == DownloadedStatusType.TimeOut) {
                // timeout, re-download
                logger.Trace("{0}: re-download...", Path.GetFileName(q.FileName));
                downloader.AddToQueue(q);
            }
            else if (status == DownloadedStatusType.Error) {
                // error, just leave
            }
        }

        private void QueueProgress(Queue q, long totalBytesToReceive, long bytesReceived, int progressPercentage) {
            logger.Trace("{0}: {2:#,0}/{1:#,0} byte(s), {3}%", Path.GetFileName(q.FileName), totalBytesToReceive, bytesReceived, progressPercentage);
        }

        // ----------------------------------------------------------------------

        // Debug Printing

        public void Log(bool moreLog = false) {
            logger.Trace(string.Format("{0}: ad count={1}", GetType().Name, AdCount));
            if (moreLog)
                adList.ForEach((x) => logger.Trace(x));
        }

    }
}
