﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using NLog;

namespace Tpig.Boosters {
    public static partial class Booster {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Delete log files with condition and number of days to keep.
        /// </summary>
        /// <param name="olderDays">Number of days to keep, such as, 30 days (enter 30)</param>
        /// <param name="baseDir">Base directory, can use environment variables</param>
        /// <param name="pattern">A regular expression to extract year, month and day from filename</param>
        /// <param name="yearIndex">Year index, default 1</param>
        /// <param name="monthIndex">Month index, default 2</param>
        /// <param name="dayIndex">Day index, default 3</param>
        public static void LogDeleter(int olderDays, string baseDir, string pattern, int yearIndex = 1, int monthIndex = 2, int dayIndex = 3) {
            baseDir = Environment.ExpandEnvironmentVariables(baseDir.TrimEnd('\\')) + '\\';
            logger.Info("log files older than {0} day(s), directory={1}", olderDays, baseDir);

            var regex = new Regex(pattern);
            var oldDay = DateTime.Now.AddDays(-olderDays);
            var files = (string[])null;
            try {
                files = Directory.GetFiles(baseDir, "*", SearchOption.AllDirectories);
            }
            catch (Exception) {
                return;
            }

            for (var i = 0; i < files.Length; i++) {
                if (files[i].StartsWith(baseDir)) {
                    var fileName = files[i].Substring(baseDir.Length);
                    var match = regex.Match(fileName);
                    if (match.Success) {
                        var y = match.Groups[yearIndex].Value;
                        var m = match.Groups[monthIndex].Value;
                        var d = match.Groups[dayIndex].Value;
                        try {
                            var yy = int.Parse(y);
                            var mm = int.Parse(m);
                            var dd = int.Parse(d);
                            var date = new DateTime(yy, mm, dd);
                            if (date < oldDay) {
                                File.Delete(files[i]);
                                logger.Info("log file deleted: {0}", fileName);
                            }
                        }
                        catch (FormatException) {
                        }
                        catch (Exception) {
                            logger.Warn("log file CANNOT be deleted: {0}", fileName);
                        }
                    }
                    else {
                        logger.Trace("log file not match: {0}", fileName);
                    }
                }
            }
        }

    }
}
