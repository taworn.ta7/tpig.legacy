﻿using System;

namespace Tpig.Boosters.SceneTimers {

    public class BasicSceneTiming : ISceneTiming {
        public TimeSpan TimeOut { get; set; }
        public TimeSpan TimeWarn { get; set; }
        public string Destination { get; set; }
    }

    public class EmptyDrawingTimeWarn : IDrawingTimeWarn {
        public void Off() { }
        public void On(TimeSpan t) { }
    }

}
