﻿using System;
using System.Windows;
using System.Windows.Threading;
using NLog;
using Tpig.Components.Navigators;

namespace Tpig.Boosters.SceneTimers {
    public partial class SceneTimer {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private DispatcherTimer timer = null;

        private NavigateMap navigateMap = null;
        private PopupHost popupHost = null;
        private bool popupMode = false;
        private bool closeFromPopup = false;

        private ISceneTimeManager sceneTimeManager = null;
        private ISceneTiming sceneTiming = null;
        private IDrawingTimeWarn drawingTimeWarn = new EmptyDrawingTimeWarn();

        private DateTime startTime = DateTime.Now;

        /// <summary>
        /// Constructor.
        /// </summary>
        public SceneTimer() {
            timer = new DispatcherTimer(DispatcherPriority.Background) {
                Interval = TimeSpan.FromMilliseconds(200),
                IsEnabled = false
            };
            timer.Tick += Tick;
        }

        // ----------------------------------------------------------------------

        /// <summary>
        /// Setup navigator's map.
        /// </summary>
        public SceneTimer Setup(NavigateMap map) {
            if (navigateMap != null)
                throw new ArgumentException("NavigateMap already set!");
            navigateMap = map;
            navigateMap.Navigating += Navigating;
            navigateMap.Navigated += Navigated;
            return this;
        }

        /// <summary>
        /// Setup popup host helper.
        /// </summary>
        public SceneTimer Setup(PopupHost host) {
            if (popupHost != null)
                throw new ArgumentException("PopupHost already set!");
            popupHost = host;
            popupHost.PopupOpened += PopupOpened;
            popupHost.PopupClosed += PopupClosed;
            return this;
        }

        /// <summary>
        /// Setup time scene manager.
        /// </summary>
        public SceneTimer Setup(ISceneTimeManager timeManager) {
            if (sceneTimeManager != null)
                throw new ArgumentException("ISceneTimeManager already set!");
            sceneTimeManager = timeManager;
            return this;
        }

        /// <summary>
        /// Setup drawing when time warning appear.
        /// </summary>
        public SceneTimer Setup(IDrawingTimeWarn drawing) {
            drawingTimeWarn = drawing;
            return this;
        }

        // ----------------------------------------------------------------------

        private void Tick(object sender, EventArgs e) {
            if (sceneTiming != null) {
                var diff = DateTime.Now - startTime;
                var timeOut = sceneTiming.TimeOut;
                var timeUsed = timeOut - diff;
                if (timeUsed <= sceneTiming.TimeWarn)
                    drawingTimeWarn.On(timeUsed);
                else
                    drawingTimeWarn.Off();
                if (diff > timeOut) {
                    if (popupMode) {
                        popupMode = false;
                        closeFromPopup = true;
                        popupHost.Detach();
                    }
                    else {
                        var nextNode = navigateMap.Map.Find(sceneTiming.Destination);
                        if (nextNode != null && nextNode != navigateMap.CurrentNode) {
                            logger.Trace("{0}: move to {1} due to timeout!", navigateMap.CurrentNode.Name, nextNode.Name);
                            navigateMap.Navigate(nextNode);
                        }
                    }
                }
            }
        }

        private void Navigating(ControlNode prevNode, ControlNode nextNode) {
            timer.IsEnabled = false;
            drawingTimeWarn.Off();
            logger.Trace("{0}: timer off", prevNode.Name);
        }

        private void Navigated(ControlNode prevNode, ControlNode nextNode) {
            if (sceneTimeManager != null) {
                sceneTiming = sceneTimeManager.Get(nextNode.Name);
                if (sceneTiming != null) {
                    logger.Trace("{0}: timer on, timeout={1}", nextNode.Name, sceneTiming.TimeOut);
                    drawingTimeWarn.Off();
                    startTime = DateTime.Now;
                    timer.IsEnabled = true;
                }
            }
        }

        private void PopupOpened(FrameworkElement element) {
            popupMode = true;

            timer.IsEnabled = false;
            drawingTimeWarn.Off();
            logger.Trace("{0}: timer pause into popup", navigateMap.CurrentNode.Name);

            if (sceneTimeManager != null) {
                var name = element.GetType().Name;
                sceneTiming = sceneTimeManager.Get(name);
                if (sceneTiming != null) {
                    logger.Trace("{0}: timer resume, timeout={1}", name, sceneTiming.TimeOut);
                    drawingTimeWarn.Off();
                    startTime = DateTime.Now;
                    timer.IsEnabled = true;
                }
            }
        }

        private void PopupClosed(FrameworkElement element) {
            popupMode = false;

            timer.IsEnabled = false;
            drawingTimeWarn.Off();
            logger.Trace("{0}: timer pause due to close popup", element.GetType().Name);

            if (closeFromPopup) {
                closeFromPopup = false;
                logger.Trace(sceneTiming);
                var nextNode = navigateMap.Map.Find(sceneTiming.Destination);
                if (nextNode != null && nextNode != navigateMap.CurrentNode) {
                    logger.Trace("{0}: auto move to {1} due to timeout!", navigateMap.CurrentNode.Name, nextNode.Name);
                    navigateMap.Navigate(nextNode);
                }
            }
            else {
                if (sceneTimeManager != null) {
                    sceneTiming = sceneTimeManager.Get(navigateMap.CurrentNode.Name);
                    if (sceneTiming != null) {
                        logger.Trace("{0}: timer resume, timeout={1}", navigateMap.CurrentNode.Name, sceneTiming.TimeOut);
                        drawingTimeWarn.Off();
                        startTime = DateTime.Now;
                        timer.IsEnabled = true;
                    }
                }
            }
        }

        // ----------------------------------------------------------------------

        public void Pause() {
            timer.IsEnabled = false;
            drawingTimeWarn.Off();
            logger.Trace("{0}: timer pause", navigateMap.CurrentNode.Name);
        }

        public void Resume() {
            logger.Trace("{0}: timer resume", navigateMap.CurrentNode.Name);
            drawingTimeWarn.Off();
            startTime = DateTime.Now;
            timer.IsEnabled = true;
        }

    }
}
