﻿using System;

namespace Tpig.Boosters.SceneTimers {

    public interface ISceneTiming {
        TimeSpan TimeOut { get; }
        TimeSpan TimeWarn { get; }
        string Destination { get; }
    }

    public interface ISceneTimeManager {
        ISceneTiming Get(string name);
    }

    public interface IDrawingTimeWarn {
        void Off();
        void On(TimeSpan t);
    }

}
