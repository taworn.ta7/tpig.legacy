﻿using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Configuration;
using NLog;
using Tpig.Components.AppConfigs;

namespace Tpig.Examples.Test {
    class Program {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args) {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables()
                .AddJsonFile("appConfigs.json", optional: false, reloadOnChange: true)
                .AddJsonFile("localConfigs.json", optional: true, reloadOnChange: true)
                .Build();
            logger.Trace("TMP={0}", config["TMP"]);
            logger.Trace("TEMP={0}", config["TEMP"]);
            logger.Trace("Section0:Key0={0}", config["Section0:Key0"]);
            logger.Trace("Section0:Key1={0}", config["Section0:Key1"]);
            var notFound = config["NotFound"];
            logger.Trace("NotFound=[{0}] ({1})", config["NotFound"], notFound == null ? "It is NULL" : "It is NOT NULL");
            MergeConfig.Instance.Add(new ConfigurationBridge(config));
            logger.Trace("Where={0}", MergeConfig.Instance.CombineReadString("where", "nowhere"));
            logger.Trace("where2={0}", MergeConfig.Instance.CombineReadString("where2"));
            logger.Trace("where3={0}", MergeConfig.Instance.CombineReadString("where3"));
            logger.Trace("");

            // add dictionary
            logger.Trace("Add Dictionary to MergeConfig");
            var dict = new Dictionary<string, string>();
            dict.Add("where2", "This is in DictionaryConfig.");
            dict.Add("where3", "---");
            MergeConfig.Instance.Add(new DictionaryConfig(dict));
            dict["where3"] = "^_^";
            logger.Trace("where2={0}", MergeConfig.Instance.CombineReadString("where2"));
            logger.Trace("where3={0}", MergeConfig.Instance.CombineReadString("where3"));
            logger.Trace("");

            // after clear
            logger.Trace("After call MergeConfig.Instance.Clear()");
            MergeConfig.Instance.Clear();
            logger.Trace("where={0}", MergeConfig.Instance.CombineReadString("where", "nowhere"));
            logger.Trace("where2={0}", MergeConfig.Instance.CombineReadString("where2"));
            logger.Trace("where3={0}", MergeConfig.Instance.CombineReadString("where3"));
        }

    }
}
