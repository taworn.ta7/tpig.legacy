﻿using System;
using System.Collections.Generic;

namespace Tpig.Components.AppConfigs {
    /// <summary>
    /// Combine many configurations into one class.
    /// </summary>
    public class MergeConfig {

        /// <summary>
        /// Singleton instance.
        /// </summary>
        public static MergeConfig Instance {
            get {
                if (instance == null) {
                    instance = new MergeConfig();
                }
                return instance;
            }
        }
        private static MergeConfig instance = null;

        private MergeConfig() {
        }

        // ----------------------------------------------------------------------

        private List<IConfigAdapter> configAdapterList = new List<IConfigAdapter>();

        /// <summary>
        /// Add configuration adapter.
        /// </summary>
        /// <param name="configAdapter"></param>
        public void Add(IConfigAdapter configAdapter) {
            if (configAdapter != null) {
                var found = configAdapterList.Find(x => x.Equals(configAdapter));
                if (found == null)
                    configAdapterList.Insert(0, configAdapter);
            }
        }

        /// <summary>
        /// Reset configuration adapter list.
        /// </summary>
        public void Clear() =>
            configAdapterList.Clear();

        // ----------------------------------------------------------------------

        /// <summary>
        /// Ini file configuration, can be null.
        /// </summary>
        public IniFile UserConfig { get; set; } = null;

        // ----------------------------------------------------------------------

        /// <summary>
        /// Combining read from configuration adapters, or ini file.
        /// </summary>
        private string CombineRead(string key, string defaultValue, string userSection) {
            foreach (var configAdapter in configAdapterList) {
                if (configAdapter.Exists(key)) {
                    return configAdapter.Read(key);
                }
            }
            if (!string.IsNullOrWhiteSpace(userSection) && UserConfig != null) {
                return UserConfig.Read(userSection, key, defaultValue);
            }
            else
                return defaultValue;
        }

        // ----------------------------------------------------------------------

        public int CombineReadInt(string key, int defaultValue = 0, string section = null) {
            var value = CombineRead(key, defaultValue.ToString(), section);
            if (value != null) {
                var result = defaultValue;
                if (int.TryParse(value, out result))
                    return result;
            }
            return defaultValue;
        }

        public long CombineReadLong(string key, long defaultValue = 0L, string section = null) {
            var value = CombineRead(key, defaultValue.ToString(), section);
            if (value != null) {
                var result = defaultValue;
                if (long.TryParse(value, out result))
                    return result;
            }
            return defaultValue;
        }

        public float CombineReadFloat(string key, float defaultValue = 0.0f, string section = null) {
            var value = CombineRead(key, defaultValue.ToString(), section);
            if (value != null) {
                var result = defaultValue;
                if (float.TryParse(value, out result))
                    return result;
            }
            return defaultValue;
        }

        public double CombineReadDouble(string key, double defaultValue = 0.0, string section = null) {
            var value = CombineRead(key, defaultValue.ToString(), section);
            if (value != null) {
                var result = defaultValue;
                if (double.TryParse(value, out result))
                    return result;
            }
            return defaultValue;
        }

        public bool CombineReadBool(string key, bool defaultValue = false, string section = null) {
            var value = CombineReadInt(key, defaultValue == false ? 0 : 1, section);
            return value != 0;
        }

        public string CombineReadString(string key, string defaultValue = "", string section = null) {
            return CombineRead(key, defaultValue, section);
        }

        public string CombineReadExpandedString(string key, string defaultValue = "", string section = null) {
            var value = CombineRead(key, defaultValue, section);
            return Environment.ExpandEnvironmentVariables(value);
        }

    }
}
