﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Tpig.Components.AppConfigs {
    /// <summary>
    /// An ini file.
    /// </summary>
    public class IniFile {

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        private string iniPath;

        public IniFile(string path) {
            iniPath = path;
        }

        public string Read(string section, string key, string defaultValue) {
            var temp = new StringBuilder(4096);
            var i = GetPrivateProfileString(section, key, defaultValue, temp, 4096, iniPath);
            return temp.ToString();
        }

        public int ReadInt(string section, string key, int defaultValue = 0) {
            var value = Read(section, key, "");
            if (value != null) {
                var result = defaultValue;
                if (int.TryParse(value, out result))
                    return result;
            }
            return defaultValue;
        }

        public long ReadLong(string section, string key, long defaultValue = 0L) {
            var value = Read(section, key, "");
            if (value != null) {
                var result = defaultValue;
                if (long.TryParse(value, out result))
                    return result;
            }
            return defaultValue;
        }

        public double ReadFloat(string section, string key, double defaultValue = 0.0) {
            var value = Read(section, key, "");
            if (value != null) {
                var result = defaultValue;
                if (double.TryParse(value, out result))
                    return result;
            }
            return defaultValue;
        }

        public bool ReadBool(string section, string key, bool defaultValue = false) {
            var value = ReadInt(section, key, defaultValue == false ? 0 : 1);
            return value != 0;
        }

        public string ReadString(string section, string key, string defaultValue = "") {
            var value = Read(section, key, "");
            if (value == null || value.Trim().Equals(""))
                value = defaultValue;
            return value;
        }

        public string ReadExpandedString(string section, string key, string defaultValue = "") {
            var value = Read(section, key, "");
            if (value == null || value.Trim().Equals(""))
                value = defaultValue;
            return Environment.ExpandEnvironmentVariables(value);
        }

        public void Write(string section, string key, string value) {
            WritePrivateProfileString(section, key, value, iniPath);
        }

        public void WriteInt(string section, string key, int value) {
            Write(section, key, value.ToString());
        }

        public void WriteLong(string section, string key, long value) {
            Write(section, key, value.ToString());
        }

        public void WriteFloat(string section, string key, double value) {
            Write(section, key, value.ToString());
        }

        public void WriteBool(string section, string key, bool value) {
            Write(section, key, (value ? 1 : 0).ToString());
        }

        public void WriteString(string section, string key, string value) {
            Write(section, key, value);
        }

    }
}
