﻿using System.Collections.Generic;

namespace Tpig.Components.AppConfigs {
    /// <summary>
    /// Provide Dictionary wrapper.
    /// </summary>
    public class DictionaryConfig : IConfigAdapter {

        private Dictionary<string, string> dict;

        public DictionaryConfig() {
            dict = new Dictionary<string, string>();
        }

        public DictionaryConfig(Dictionary<string, string> d) {
            if (d != null)
                dict = d;
            else
                dict = new Dictionary<string, string>();
        }

        public void SetDictionary(Dictionary<string, string> d) {
            if (d != null)
                dict = d;
            else
                dict = new Dictionary<string, string>();
        }

        public bool Exists(string key) {
            return dict.ContainsKey(key);
        }

        public string Read(string key) {
            return dict[key];
        }

    }
}
