﻿namespace Tpig.Components.AppConfigs {
    /// <summary>
    /// Provide interface to connect with real configuration system.
    /// </summary>
    public interface IConfigAdapter {

        /// <summary>
        /// Check if key is exists.
        /// </summary>
        bool Exists(string key);

        /// <summary>
        /// Read value with key.
        /// </summary>
        string Read(string key);

    }
}
