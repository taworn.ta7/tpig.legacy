﻿using Microsoft.Extensions.Configuration;

namespace Tpig.Components.AppConfigs {
    /// <summary>
    /// Provide IConfiguration wrapper.
    /// </summary>
    public class ConfigurationBridge : IConfigAdapter {

        public IConfiguration Config { get; private set; } = null;

        public ConfigurationBridge(IConfiguration config) {
            Config = config;
        }

        public bool Exists(string key) {
            return Config[key] != null;
        }

        public string Read(string key) {
            return Config[key];
        }

    }
}
