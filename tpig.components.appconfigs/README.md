**Tpig.Components.AppConfigs**
==============================

An application's configurations merged into one component.


## Requirements

- .NET version 6.0


## Thank You

- NLog, https://nlog-project.org
- gitignore, https://github.com/github/gitignore and https://gitignore.io


## NuGet

You can retrieve package at https://www.nuget.org/packages/Tpig.Components.AppConfigs.


## Last

Sorry, but I'm not good at English. T_T

Taworn Ta.

