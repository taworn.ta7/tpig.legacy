﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using InputSimulatorStandard;

namespace Tpig.Components.SoftKeyboards.KeyboardLayouts {
    public class KeyboardViewModel : INotifyPropertyChanged {

        public InputSimulator Input { get; private set; } = null;

        public KeyboardViewModel(InputSimulator i) => Input = i;

        // ----------------------------------------------------------------------

        // Normal Key

        private ICommand normalKeyCommand = null;
        public ICommand NormalKeyCommand {
            get {
                if (normalKeyCommand == null)
                    normalKeyCommand = new Internals.RelayCommand(o => NormalKeyDelegate?.Invoke(o));
                return normalKeyCommand;
            }
        }
        public Action<object> NormalKeyDelegate { get; set; } = null;

        // Special Key

        private ICommand specialKeyCommand = null;
        public ICommand SpecialKeyCommand {
            get {
                if (specialKeyCommand == null)
                    specialKeyCommand = new Internals.RelayCommand(o => SpecialKeyDelegate?.Invoke(o));
                return specialKeyCommand;
            }
        }
        public Action<object> SpecialKeyDelegate { get; set; } = null;

        // ----------------------------------------------------------------------

        // Property Changed

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

    }
}
