﻿using System.Collections.Generic;

namespace Tpig.Components.SoftKeyboards.KeyboardLayouts {
    public partial class KeyboardControl {

        public void AddThaiLanguage() {
            var d = new Dictionary<char, char>();
            d.Add('`', '_');
            d.Add('1', 'ๅ');
            d.Add('2', '/');
            d.Add('3', '-');
            d.Add('4', 'ภ');
            d.Add('5', 'ถ');
            d.Add('6', 'ุ');
            d.Add('7', 'ึ');
            d.Add('8', 'ค');
            d.Add('9', 'ต');
            d.Add('0', 'จ');
            d.Add('-', 'ข');
            d.Add('=', 'ช');
            d.Add('~', '%');
            d.Add('!', '+');
            d.Add('@', '๑');
            d.Add('#', '๒');
            d.Add('$', '๓');
            d.Add('%', '๔');
            d.Add('^', 'ู');
            d.Add('&', '฿');
            d.Add('*', '๕');
            d.Add('(', '๖');
            d.Add(')', '๗');
            d.Add('_', '๘');
            d.Add('+', '๙');

            d.Add('q', 'ๆ');
            d.Add('w', 'ไ');
            d.Add('e', 'ำ');
            d.Add('r', 'พ');
            d.Add('t', 'ะ');
            d.Add('y', 'ั');
            d.Add('u', 'ี');
            d.Add('i', 'ร');
            d.Add('o', 'น');
            d.Add('p', 'ย');
            d.Add('[', 'บ');
            d.Add(']', 'ล');
            d.Add('\\', 'ฃ');
            d.Add('Q', '๐');
            d.Add('W', '"');
            d.Add('E', 'ฎ');
            d.Add('R', 'ฑ');
            d.Add('T', 'ธ');
            d.Add('Y', 'ํ');
            d.Add('U', '๊');
            d.Add('I', 'ณ');
            d.Add('O', 'ฯ');
            d.Add('P', 'ญ');
            d.Add('{', 'ฐ');
            d.Add('}', ',');
            d.Add('|', 'ฅ');

            d.Add('a', 'ฟ');
            d.Add('s', 'ห');
            d.Add('d', 'ก');
            d.Add('f', 'ด');
            d.Add('g', 'เ');
            d.Add('h', '้');
            d.Add('j', '่');
            d.Add('k', 'า');
            d.Add('l', 'ส');
            d.Add(';', 'ว');
            d.Add('\'', 'ง');
            d.Add('A', 'ฤ');
            d.Add('S', 'ฆ');
            d.Add('D', 'ฏ');
            d.Add('F', 'โ');
            d.Add('G', 'ฌ');
            d.Add('H', '็');
            d.Add('J', '๋');
            d.Add('K', 'ษ');
            d.Add('L', 'ศ');
            d.Add(':', 'ซ');
            d.Add('"', '.');

            d.Add('z', 'ผ');
            d.Add('x', 'ป');
            d.Add('c', 'แ');
            d.Add('v', 'อ');
            d.Add('b', 'ิ');
            d.Add('n', 'ื');
            d.Add('m', 'ท');
            d.Add(',', 'ม');
            d.Add('.', 'ใ');
            d.Add('/', 'ฝ');
            d.Add('Z', '(');
            d.Add('X', ')');
            d.Add('C', 'ฉ');
            d.Add('V', 'ฮ');
            d.Add('B', 'ฺ');
            d.Add('N', '์');
            d.Add('M', '?');
            d.Add('<', 'ฒ');
            d.Add('>', 'ฬ');
            d.Add('?', 'ฦ');

            keyboardMapping.Add("th", new KeyboardMapping {
                Name = "THA",
                Mapping = d
            });
            keyboardList.Add("th");
        }

    }
}
