﻿using System.Windows;

namespace Tpig.Components.SoftKeyboards {
    public static class SoftKeyboardUtilities {

        public static SoftKeyboardControl FindSoftKeyboard(FrameworkElement control) {
            var window = Window.GetWindow(control);
            return Internals.Helpful.GetDescendantByType<SoftKeyboardControl>(window);
        }

    }
}
