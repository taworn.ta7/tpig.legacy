**Tpig.Components.SoftKeyboards**
=================================

.NET WPF component for virtual keyboards.


## Examples

First, add this line in xaml:

	xmlns:kb="clr-namespace:Tpig.Components.SoftKeyboards;assembly=Tpig.Components.SoftKeyboards"
	
Then, put this line to instance soft-keyboard, this must be top:

    <kb:SoftKeyboardControl x:Name="SoftKeyboardControl"/>
    
Then, there are 2 ways to attach soft-keyboards:

- use attach property, which is simple but less configure
- use behavior

When you want to use attach property, here:

	<TextBox kb:AutoSoftKeyboardBehavior.UseSoftKeyboard="True"/>
    
If you want to use behavior, add this line:

	xmlns:be="http://schemas.microsoft.com/xaml/behaviors"

Then:

	<TextBox>
		<be:Interaction.Behaviors>
			<kb:SoftKeyboardBehavior SoftKeyboardType="International"/>
		</be:Interaction.Behaviors>
	</TextBox>

You can set SoftKeyboardType as International, EnglishOnly and Numeric.


## Language Extensions

You can add language, like this:

	Loaded += (sender, e) => {
		Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)(() => {
			SoftKeyboardControl.AddThaiLanguage();
		}));
	};

Sorry, I can use English and Thai only.  If you want another language, you have to fork.
See SoftKeyboardControl.AddThaiLanguage() as basis.


## Requirements

- .NET version 6.0


## Thank You

- GetDescendantByType, I can't remember this function, sorry T_T
- InputSimulator, https://archive.codeplex.com/?p=inputsimulator
- InputSimulatorStandard, https://github.com/GregsStack/InputSimulatorStandard
- Microsoft.Xaml.Behaviors.Wpf, https://github.com/Microsoft/XamlBehaviorsWpf
- NLog, https://nlog-project.org
- gitignore, https://github.com/github/gitignore and https://gitignore.io


## NuGet

You can retrieve package at https://www.nuget.org/packages/Tpig.Components.SoftKeyboards.


## Last

Sorry, but I'm not good at English. T_T

Taworn Ta.

