﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace Tpig.Examples.Test {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        public MainWindow() {
            InitializeComponent();

            Loaded += (sender, e) => {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)(() => {
                    SoftKeyboardControl.AddThaiLanguage();
                }));
            };
        }

    }
}
