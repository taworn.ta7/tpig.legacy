**Tpig.Helpers**
================

A packaged useful functions and classes libraries.


## Requirements

- .NET version 6.0


## Thank You

- ByteArrayToStructure, https://stackoverflow.com/questions/2871/reading-a-c-c-data-structure-in-c-sharp-from-a-byte-array, https://stackoverflow.com/users/42/coincoin, https://stackoverflow.com/users/424129/ed-plunkett and https://stackoverflow.com/users/184528/cdiggins
- ObjectExtensions, https://github.com/Burtsev-Alexey/net-object-deep-copy
- StringCipher, https://stackoverflow.com/users/57477/craigtp and https://stackoverflow.com/questions/10168240/encrypting-decrypting-a-string-in-c-sharp/10177020#10177020
- FindVisualChildren, https://stackoverflow.com/users/73509/bryce-kahle and https://stackoverflow.com/questions/974598/find-all-controls-in-wpf-window-by-type
- GetDescendantByType, I can't remember this function, sorry T_T
- RelayCommand, https://stackoverflow.com/questions/22285866/why-relaycommand and https://stackoverflow.com/users/632337/rohit-vats
- Loading image, https://loading.io
- gitignore, https://github.com/github/gitignore and https://gitignore.io


## NuGet

You can retrieve package at https://www.nuget.org/packages/Tpig.Helpers and https://www.nuget.org/packages/Tpig.Helpers.CommonUi. 


## Last

Sorry, but I'm not good at English. T_T

Taworn Ta.

