﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using Tpig.Helpers.CommonUi;

namespace Tpig.Examples.TestSimpleAnimation {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private static SimpleAnimation sprite = null;
        private SimpleAnimationRenderer renderer = null;

        public MainWindow() {
            InitializeComponent();

            if (sprite == null) {
                sprite = new SimpleAnimation {
                    DefaultTimeOut = TimeSpan.FromMilliseconds(50),
                    IsSharedImages = true
                };
                var resPathTemplate = "/Tpig.Examples.TestSimpleAnimation;component/Resources/frame-{0}.png";
                for (var i = 0; i < 20; i++) {
                    var number = string.Format("{0:00}", i);
                    var resPath = string.Format(resPathTemplate, number);
                    var image = new BitmapImage(new Uri(resPath, UriKind.RelativeOrAbsolute));
                    sprite.Add(image);
                }
            }

            renderer = new SimpleAnimationRenderer(WaitImage) {
                Animation = sprite,
                End = SimpleAnimationRenderer.PlayEnd.Repeat
            };

            StartButton.Click += (sender, e) => renderer.Play();
            StopButton.Click += (sender, e) => renderer.Stop();
        }

    }
}
