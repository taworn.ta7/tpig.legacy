﻿using System.Reflection;

namespace Tpig.Helpers {
    /// <summary>
    /// Simple application path retrieval.
    /// </summary>
    public class AppPath {

        public string FullName { get; private set; }
        public string PathName { get; private set; }
        public string FileName { get; private set; }
        public string OnlyName { get; private set; }
        public string BaseName { get; private set; }

        public static AppPath Instance {
            get {
                if (instance == null)
                    instance = new AppPath();
                return instance;
            }
        }
        private static AppPath instance = null;

        private AppPath() {
            FullName = Assembly.GetEntryAssembly().Location;
            PathName = System.IO.Path.GetDirectoryName(FullName);
            FileName = System.IO.Path.GetFileName(FullName);
            OnlyName = System.IO.Path.GetFileNameWithoutExtension(FullName);
            BaseName = string.Format("pack://application:,,,/{0};component", OnlyName);
        }

        public static string Path() {
            return Instance.PathName;
        }

        public static string Path(string relativePath) {
            return System.IO.Path.Combine(Instance.PathName, relativePath);
        }

    }
}
