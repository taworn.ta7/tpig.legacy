﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Tpig.Helpers.CommonUi.Converters {

    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityHiddenConverter : IValueConverter {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var o = (bool)value;
            return o ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            var o = (Visibility)value;
            return o == Visibility.Visible ? true : false;
        }

    }

    // ----------------------------------------------------------------------

    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityCollapsedConverter : IValueConverter {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var o = (bool)value;
            return o ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            var o = (Visibility)value;
            return o == Visibility.Visible ? true : false;
        }

    }

}
