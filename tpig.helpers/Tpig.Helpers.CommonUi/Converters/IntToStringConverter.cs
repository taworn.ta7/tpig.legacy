﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Tpig.Helpers.CommonUi.Converters {

    [ValueConversion(typeof(int), typeof(string))]
    public class IntToStringConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var o = (int)value;
            return o.ToString();
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            var s = (string)value;
            var o = 0;
            if (int.TryParse(s, out o))
                return o;
            else
                return 0;
        }
    }

    // ----------------------------------------------------------------------

    [ValueConversion(typeof(int?), typeof(string))]
    public class IntToStringOptionalConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var o = (int?)value;
            return o.ToString();
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            var s = (string)value;
            var o = 0;
            if (int.TryParse(s, out o))
                return o;
            else
                return null as int?;
        }
    }

}
