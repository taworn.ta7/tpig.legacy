export = MergeConfigEx;
declare class MergeConfigEx {
	constructor(options?: any);
	config: any;
	get: (key: any, def?: any) => any;
	set: (key: any, value: any) => MergeConfigEx;
	merge: (sources: any) => MergeConfigEx;
	env: (whitelist: any) => MergeConfigEx;
	argv: (whitelist: any) => MergeConfigEx;
	file: (file: any) => MergeConfigEx;
}
