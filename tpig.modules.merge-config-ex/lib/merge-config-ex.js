'use strict'
const path = require('path')
const fs = require('fs')
const CSON = require('cson-parser')
const Config = require('merge-config')

class MergeConfigEx {

    constructor(options) {
        this.config = new Config(options)
    }

    get = (key, def) => {
        const value = this.config.get(key)
        return value !== undefined ? value : def
    }

    set = (key, value) => {
        this.config.set(key, value)
        return this
    }

    merge = (sources) => {
        this.config.merge(sources)
        return this
    }

    env = (whitelist) => {
        this.config.env(whitelist)
        return this
    }

    argv = (whitelist) => {
        this.config.argv(whitelist)
        return this
    }

    file = (file) => {
        if (path.extname(file) === '.cson') {
            const text = fs.readFileSync(file)
            const json = CSON.parse(text)
            this.config.merge(json)
        }
        else {
            this.config.file(file)
        }
        return this
    }

}

module.exports = MergeConfigEx
