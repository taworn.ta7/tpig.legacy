This package is legacy!

# merge-config-ex

A module configurations merged into one component.

This library extend from [merge-config](https://www.npmjs.com/package/merge-config).  Their are 2 changes:

* get(key, def): optionally have default value if key not found
* file(path): this function have ability to load CSON, along with JSON and YAML

# APIs

## Contructor

Create configuration.

	const Config = require('merge-config-ex')
	const config = new Config()

## get(key, def)

Get 'value' from 'key'.  If 'key' not found, return 'def'.

	config.get('KEY', 'default value')

## set(key, value)

Set 'key' and 'value' into configuration.

	config.set('KEY', 'value')

## merge(sources)

Copy from 'sources' object into configuration.

	config.merge({ KEY1: 'value1', KEY2: 'value2' })

## env(whitelist)

Copy from environment variables into configuration.  All variables copied will convert to camelCase.

	config.env(['TEMP', 'TMP'])

copy just TEMP and TMP

	config.env()

copy all

## argv(whitelist)

Copy from argument variables from command line into configuration.  All variables copied will put into array.

	config.argv()

## file(file)

Copy from file into configuration.  The configuration support JSON, YAML and CSON.

	const path = require('path')
	config.file(path.join(__dirname, 'configs/config.yaml'))

# Requirements

* Node
* merge-config
* cson-parser

# Thank You

* merge-config, https://www.npmjs.com/package/merge-config
* cson-parser, https://www.npmjs.com/package/cson-parser
* gitignore, https://github.com/github/gitignore and https://gitignore.io

# NPM

You can retrieve package at [NPM](https://www.npmjs.com/package/merge-config-ex).

# Last

About use readFileSync() without async.  I think this library is configuation and I must load just one.
And sorry, but I'm not good at English. T_T

Taworn Ta.
