**Tpig.Components.Downloaders**
===============================

.NET component to facade queue of downloads.


## How to Use

Prepare:

    var downloader = new Downloader();
    downloader.AddToQueue(url1, file_to_save1, Callback, Progress);
    downloader.AddToQueue(url2, file_to_save2, Callback, Progress, priority: true);

When you ready:

    downloader.Start();
    while (!downloader.Finished) {
        Thread.Sleep(1000);
    }

Event handlings, not necessary:

    private void Callback(Queue q, DownloadedStatusType status) {
        logger.Debug("{0}: {1}, result={2}", Path.GetFileName(q.FileName), q.Url, status);
    }

    private void Progress(Queue q, long totalBytesToReceive, long bytesReceived, int progressPercentage) {
        logger.Trace("{0}: {2:#,0}/{1:#,0} byte(s), {3}%", Path.GetFileName(q.FileName), totalBytesToReceive, bytesReceived, progressPercentage);
    }


## Requirements

- .NET version 6.0


## Thank You

- NLog, https://nlog-project.org
- gitignore, https://github.com/github/gitignore and https://gitignore.io


## NuGet

You can retrieve package at https://www.nuget.org/packages/Tpig.Components.Downloaders.


## Last

Sorry, but I'm not good at English. T_T

Taworn Ta.

