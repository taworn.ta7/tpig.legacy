﻿using System;
using System.ComponentModel;
using System.Net;
using System.Threading;
using NLog;

namespace Tpig.Examples.Test {
    class Program {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args) {
            var program = new Program();
            program.Run();
        }

        // ----------------------------------------------------------------------

        private void Run() {
            var client = new WebClient();
            client.DownloadFileCompleted += DownloadFileCompleted;
            client.DownloadProgressChanged += DownloadProgressChanged;
            client.DownloadFileAsync(new Uri("https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v7.8.2/npp.7.8.2.bin.7z"), "npp.bin");

            Console.WriteLine("press ESC key to cancel...");
            while (client.IsBusy) {
                if (Console.KeyAvailable) {
                    var key = Console.ReadKey(false);
                    if (key.Key == ConsoleKey.Escape) {
                        client.CancelAsync();
                    }
                }
                Thread.Sleep(100);
            }

            logger.Trace("done :)");
        }

        private void DownloadFileCompleted(object sender, AsyncCompletedEventArgs e) {
            if (e.Cancelled) {
                logger.Trace("timeout");
            }
            else {
                if (e.Error != null) {
                    logger.Trace("exception: {0}", e.Error.Message);
                }
                else {
                    logger.Trace("download ok");
                }
            }
        }

        private void DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e) {
            logger.Trace("{0:#,0}/{1:#,0} byte(s), {2}%", e.BytesReceived, e.TotalBytesToReceive, e.ProgressPercentage);
        }

    }
}
