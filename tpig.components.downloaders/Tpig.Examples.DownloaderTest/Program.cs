﻿using System.IO;
using System.Threading;
using NLog;
using Tpig.Components.Downloaders;

namespace Tpig.Examples.DownloaderTest {
    class Program {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args) {
            var program = new Program();
            program.Run();
        }

        // ----------------------------------------------------------------------

        private void Run() {
            var downloader = new Downloader();
            downloader.AddToQueue("https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v7.8.2/npp.7.8.2.bin.7zzz",
                                  "npperror.bin", Callback, Progress);
            downloader.AddToQueue("https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v7.8.2/npp.7.8.2.bin.7z",
                                  "npp.bin", Callback, Progress, priority: true);

            downloader.Start();
            while (!downloader.Finished) {
                Thread.Sleep(1000);
            }
        }

        private void Callback(Queue q, DownloadedStatusType status) {
            logger.Debug("{0}: {1}, result={2}", Path.GetFileName(q.FileName), q.Url, status);
        }

        private void Progress(Queue q, long totalBytesToReceive, long bytesReceived, int progressPercentage) {
            logger.Trace("{0}: {2:#,0}/{1:#,0} byte(s), {3}%", Path.GetFileName(q.FileName), totalBytesToReceive, bytesReceived, progressPercentage);
        }

    }
}
