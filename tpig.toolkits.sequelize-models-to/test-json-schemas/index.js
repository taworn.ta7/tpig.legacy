'use strict'
const path = require('path')
const convert = require('sequelize-models-to-json-schemas')
const db = require('./db')

// running
const run = async () => {
    try {
        await db.sequelize.sync({ force: true })

        convert.to(path.join(__dirname, 'schemas.outputs'), [
            { model: db.Strings, file: 'Strings.json' },
            { model: db.Numerics, file: 'Numerics.json' },
            { model: db.Dates, file: 'Dates.json' },
            { model: db.Others, file: 'Others.json' }
        ])

        process.exit(0)
    }
    catch (ex) {
        console.log(ex)
        process.exit(1)
    }
}
run()
