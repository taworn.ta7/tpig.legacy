'use strict'
const { DataTypes } = require('sequelize')
const db = require('./sequelize')

module.exports = db.define('Dates', {
    date: DataTypes.DATE,
    dateOnly: DataTypes.DATEONLY
}, {
    tableName: 'to-json-schemas-dates'
})
