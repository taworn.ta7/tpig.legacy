'use strict'
const { DataTypes } = require('sequelize')
const db = require('./sequelize')

module.exports = db.define('Numerics', {
    bool: DataTypes.BOOLEAN,
    integer: DataTypes.INTEGER,
    int5: DataTypes.INTEGER(5),
    bigInt: DataTypes.BIGINT,

    float: DataTypes.FLOAT,
    float5: DataTypes.FLOAT(5),
    float5_2: DataTypes.FLOAT(5, 2),

    double: DataTypes.DOUBLE,

    decimal: DataTypes.DECIMAL,
    decimal10: DataTypes.DECIMAL(10),
    decimal10_2: DataTypes.DECIMAL(10, 2),

    limit: {
        type: DataTypes.INTEGER,
        validate: {
            min: 5,
            max: 55555
        }
    }
}, {
    tableName: 'to-json-schemas-numerics'
})
