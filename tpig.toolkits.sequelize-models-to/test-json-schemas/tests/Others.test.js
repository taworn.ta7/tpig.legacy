'use strict'
const schemas = require('../schemas')

describe("testing Others schema", () => {

    test("uuid: error", () => {
        const json = {
            uuid: 0
        }
        const result = schemas.other(json)
        expect(result).toBe(false)
    })

    test("uuid: invalid", () => {
        const json = {
            uuid: 'ab cd'
        }
        const result = schemas.other(json)
        expect(result).toBe(false)
    })

    test("uuid: ok", () => {
        const json = {
            uuid: 'abcd+WXYZ-1234*5678/@#$%'
        }
        const result = schemas.other(json)
        expect(result).toBe(true)
    })

    // ----------------------------------------------------------------------

    test("enum: invalid", () => {
        const json = {
            enum: 'z'
        }
        const result = schemas.other(json)
        expect(result).toBe(false)
    })

    test("enum: ok", () => {
        const json = {
            enum: 'a'
        }
        const result = schemas.other(json)
        expect(result).toBe(true)
    })

})
