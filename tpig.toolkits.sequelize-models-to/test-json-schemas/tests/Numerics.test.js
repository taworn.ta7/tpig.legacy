'use strict'
const schemas = require('../schemas')

describe("testing Numerics schema", () => {

    test("bool: true or false only", () => {
        const json = {
            bool: 0
        }
        const result = schemas.numeric(json)
        expect(result).toBe(false)
    })

    test("bool: ok", () => {
        const json = {
            bool: true
        }
        const result = schemas.numeric(json)
        expect(result).toBe(true)
    })

    // ----------------------------------------------------------------------

    test("precision: break min", () => {
        const json = {
            int5: -999999
        }
        const result = schemas.numeric(json)
        expect(result).toBe(false)
    })

    test("precision: break max", () => {
        const json = {
            int5: 999999
        }
        const result = schemas.numeric(json)
        expect(result).toBe(false)
    })

    test("precision: ok", () => {
        const json = {
            int5: 55555
        }
        const result = schemas.numeric(json)
        expect(result).toBe(true)
    })

    // ----------------------------------------------------------------------

    test("precision decimal: break min", () => {
        const json = {
            decimal10_2: -99999999.999
        }
        const result = schemas.numeric(json)
        expect(result).toBe(false)
    })

    test("precision decimal: break max", () => {
        const json = {
            decimal10_2: 99999999.999
        }
        const result = schemas.numeric(json)
        expect(result).toBe(false)
    })

    test("precision decimal: ok", () => {
        const json = {
            decimal10_2: 99999999.99
        }
        const result = schemas.numeric(json)
        expect(result).toBe(true)
    })

    // ----------------------------------------------------------------------

    test("limit: break min", () => {
        const json = {
            limit: 0
        }
        const result = schemas.numeric(json)
        expect(result).toBe(false)
    })

    test("limit: break max", () => {
        const json = {
            limit: 66666
        }
        const result = schemas.numeric(json)
        expect(result).toBe(false)
    })

    test("limit: ok", () => {
        const json = {
            limit: 555
        }
        const result = schemas.numeric(json)
        expect(result).toBe(true)
    })

})
