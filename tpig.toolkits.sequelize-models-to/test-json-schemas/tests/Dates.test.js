'use strict'
const schemas = require('../schemas')

describe("testing Dates schema", () => {

    test("date: error", () => {
        const json = {
            date: 0
        }
        const result = schemas.date(json)
        expect(result).toBe(false)
    })

    test("date: invalid", () => {
        const json = {
            date: '2020-13-31T11:22:33.4444Z'
        }
        const result = schemas.date(json)
        expect(result).toBe(false)
    })

    test("date: ok", () => {
        const json = {
            date: '2020-12-31T11:22:33.4444Z'
        }
        const result = schemas.date(json)
        expect(result).toBe(true)
    })

    // ----------------------------------------------------------------------

    test("date only: error", () => {
        const json = {
            dateOnly: 0
        }
        const result = schemas.date(json)
        expect(result).toBe(false)
    })

    test("date only: invalid", () => {
        const json = {
            dateOnly: '2020-13-31'
        }
        const result = schemas.date(json)
        expect(result).toBe(false)
    })

    test("date only: valid but have time", () => {
        const json = {
            dateOnly: '2020-12-31T11:22:33.4444Z'
        }
        const result = schemas.date(json)
        expect(result).toBe(false)
    })

    test("date only: ok", () => {
        const json = {
            dateOnly: '2020-12-31'
        }
        const result = schemas.date(json)
        expect(result).toBe(true)
    })

})
