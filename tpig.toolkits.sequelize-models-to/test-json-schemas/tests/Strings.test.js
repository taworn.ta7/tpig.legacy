'use strict'
const schemas = require('../schemas')

describe("testing Strings schema", () => {

    test("limit: break maxLength", () => {
        const json = {
            limit: "John Doe"
        }
        const result = schemas.string(json)
        expect(result).toBe(false)
    })

    test("limit: break maxLength", () => {
        const json = {
            limit2: "John Doe"
        }
        const result = schemas.string(json)
        expect(result).toBe(false)
    })

    test("limit: break minLength", () => {
        const json = {
            limit2: ""
        }
        const result = schemas.string(json)
        expect(result).toBe(false)
    })

    test("limit: ok", () => {
        const json = {
            limit2: "John"
        }
        const result = schemas.string(json)
        expect(result).toBe(true)
    })

    // ----------------------------------------------------------------------

    test("enum: invalid string", () => {
        const json = {
            enum: "d"
        }
        const result = schemas.string(json)
        expect(result).toBe(false)
    })

    test("enum: ok", () => {
        const json = {
            enum: "c"
        }
        const result = schemas.string(json)
        expect(result).toBe(true)
    })

    test("enum not: invalid string", () => {
        const json = {
            enumNot: "a"
        }
        const result = schemas.string(json)
        expect(result).toBe(false)
    })

    test("enum not: ok", () => {
        const json = {
            enumNot: "z"
        }
        const result = schemas.string(json)
        expect(result).toBe(true)
    })

    // ----------------------------------------------------------------------

    test("pattern: invalid character(s)", () => {
        const json = {
            pattern: "+-*/"
        }
        const result = schemas.string(json)
        expect(result).toBe(false)
    })

    test("pattern: ok", () => {
        const json = {
            pattern: "abcdefghij0123456789"
        }
        const result = schemas.string(json)
        expect(result).toBe(true)
    })

    test("pattern not: invalid character(s)", () => {
        const json = {
            patternNot: "abcdefghij0123456789"
        }
        const result = schemas.string(json)
        expect(result).toBe(false)
    })

    test("pattern not: ok", () => {
        const json = {
            patternNot: "+-*/"
        }
        const result = schemas.string(json)
        expect(result).toBe(true)
    })

    // ----------------------------------------------------------------------

    test("isAlpha: invalid character(s)", () => {
        const json = {
            isAlpha: "abcABC012"
        }
        const result = schemas.string(json)
        expect(result).toBe(false)
    })

    test("isAlpha: ok", () => {
        const json = {
            isAlpha: "abcABC"
        }
        const result = schemas.string(json)
        expect(result).toBe(true)
    })

    test("isAlphanumeric: invalid character(s)", () => {
        const json = {
            isAlphanumeric: "abcABC012+-*/"
        }
        const result = schemas.string(json)
        expect(result).toBe(false)
    })

    test("isAlphanumeric: ok", () => {
        const json = {
            isAlphanumeric: "abcABC012"
        }
        const result = schemas.string(json)
        expect(result).toBe(true)
    })

    // ----------------------------------------------------------------------

    test("isInt: invalid character(s)", () => {
        const json = {
            isInt: "+012.0"
        }
        const result = schemas.string(json)
        expect(result).toBe(false)
    })

    test("isInt: ok", () => {
        const json = {
            isInt: "-012"
        }
        const result = schemas.string(json)
        expect(result).toBe(true)
    })

    test("isFloat: invalid character(s)", () => {
        const json = {
            isFloat: "+012.0.0"
        }
        const result = schemas.string(json)
        expect(result).toBe(false)
    })

    test("isFloat: ok", () => {
        const json = {
            isFloat: "-012.0"
        }
        const result = schemas.string(json)
        expect(result).toBe(true)
    })

    // ----------------------------------------------------------------------

    test("notEmpty: empty error", () => {
        const json = {
            notEmpty: ""
        }
        const result = schemas.string(json)
        expect(result).toBe(false)
    })

    test("notEmpty: ok", () => {
        const json = {
            notEmpty: "^-^"
        }
        const result = schemas.string(json)
        expect(result).toBe(true)
    })

})
