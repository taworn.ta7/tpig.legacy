'use strict'
const Ajv = require('ajv').default
const ajv = new Ajv({
    metaSchema: require('ajv/lib/refs/json-schema-draft-07.json'),
    schemas: [
        require('./Strings.json'),
        require('./Numerics.json'),
        require('./Dates.json'),
        require('./Others.json')
    ]
})

const addFormats = require('ajv-formats')
addFormats(ajv)

const baseUri = 'https://sequelize-models-to/to-json-schemas/'
module.exports = {
    string: ajv.compile({ '$ref': `${baseUri}Strings.json#/definitions/string` }),
    numeric: ajv.compile({ '$ref': `${baseUri}Numerics.json#/definitions/numeric` }),
    date: ajv.compile({ '$ref': `${baseUri}Dates.json#/definitions/date` }),
    other: ajv.compile({ '$ref': `${baseUri}Others.json#/definitions/other` })
}
