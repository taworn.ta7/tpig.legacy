'use strict'
const path = require('path')
const fs = require('fs')

/*
 * to(outputDir, list)
 * convert sequelize model to Joi files
 */
const to = (outputDir, list, options) => {
    for (let i = 0; i < list.length; i++) {
        const item = list[i]
        toFile(item.model, path.join(outputDir, item.file), options)
    }
}

/*
 * toFile(model, file)
 * convert sequelize model to Joi file
 */
const toFile = (model, file, options) => {
    const text = toJs(model, options)
    fs.writeFileSync(file, text)
}

/*
 * toJs(model)
 * convert sequelize model to Joi
 */
const toJs = (model, options) => {
    const properties = {}
    for (let key in model.rawAttributes) {
        const attr = model.rawAttributes[key]
        properties[key] = convertProperty(key, attr)
    }

    const schema = {
        [model.name]: properties
    }
    return schemaToText(schema, options)
}

// ----------------------------------------------------------------------

const schemaToText = (schema, options) => {
    let text = []
    const name = Object.keys(schema)[0]
    text.push(`${name}: Joi.object({`)
    text.push(`\n\n`)

    const properties = schema[name]
    for (let key in properties) {
        text.push(`\t${key}: Joi`)
        const values = properties[key]

        if (values.string)
            text.push(`.string()`)
        if (values.boolean)
            text.push(`.boolean()`)
        if (values.number)
            text.push(`.number()`)
        if (values.integer)
            text.push(`.integer()`)
        if (values.date)
            text.push(`.date()`)
        if (values.iso)
            text.push(`.iso()`)

        if (typeof values.min === 'number' || typeof values.max === 'number') {
            text.push(`\n\t\t`)
            if (typeof values.min === 'number')
                text.push(`.min(${values.min})`)
            if (typeof values.max === 'number')
                text.push(`.max(${values.max})`)
        }

        if (values.enum) {
            text.push(`\n\t\t`)
            const params = values.enum.map((item) => JSON.stringify(item))
            text.push(`.valid(${params})`)
        }

        if (values.pattern) {
            let params = values.pattern
            text.push(`\n\t\t`)
            text.push(`.pattern(/${params}/)`)
        }

        let ret = false
        if (values.allow) {
            let params = 'null'
            if (values.string)
                params += ', \'\''
            if (!ret)
                text.push(`\n\t\t`)
            ret = true
            text.push(`.allow(${params})`)
        }
        else if (values.string) {
            if (!values.min || (typeof values.min === 'number' && values.min <= 0)) {
                if (!ret)
                    text.push(`\n\t\t`)
                ret = true
                text.push(`.allow('')`)
            }
        }
        if (options && options.useRequired) {
            if (values.required) {
                if (!ret)
                    text.push(`\n\t\t`)
                ret = true
                text.push(`.required()`)
            }
        }

        text.push(`,\n\n`)
    }

    text.push(`})`)
    return text.join('')
}

const convertProperty = (key, attr) => {
    const result = {}

    switch (attr.type.key) {
        case 'STRING':
            result.string = true
            result.max = attr.type._length
            break

        case 'TEXT':
            result.string = true
            if (typeof attr.type.options.length === 'undefined')
                result.max = 0xffff
            else {
                const size = attr.type.options.length.toLowerCase()
                if (size === 'tiny')
                    result.max = 0xff
                if (size === '')
                    result.max = 0xffff
                else if (size === 'medium')
                    result.max = 0xffffff
                else if (size === 'long')
                    result.max = 0xffffffff
            }
            break

        case 'BOOLEAN':
            result.boolean = true
            break

        case 'INTEGER':
        case 'BIGINT':
            result.number = true
            result.integer = true
            if (typeof attr.type.options.length !== 'undefined') {
                const value = precisionToValue(attr.type.options.length)
                result.min = -value
                result.max = value
            }
            break

        case 'REAL':
        case 'FLOAT':
        case 'DOUBLE':
        case 'DOUBLE PRECISION':
            result.number = true
            if (typeof attr.type.options.length !== 'undefined') {
                const value = precisionToValue(attr.type.options.length, attr.type.options.decimals)
                result.min = -value
                result.max = value
            }
            break

        case 'DECIMAL':
            result.number = true
            if (typeof attr.type.options.precision !== 'undefined') {
                const value = precisionToValue(attr.type.options.precision, attr.type.options.scale)
                result.min = -value
                result.max = value
            }
            break

        case 'DATE':
            result.date = true
            result.iso = true
            break

        case 'DATEONLY':
            result.date = true
            result.iso = true
            break

        case 'UUID':
            result.string = true
            result.pattern = '^[a-zA-Z0-9+\\-*/@#$%]+$'
            break

        case 'ENUM':
            result.string = true
            result.enum = attr.values
            break
    }

    if (typeof attr.allowNull === 'undefined' || attr.allowNull) {
        result.allow = true
    }
    if (attr.allowNull === false) {
        result.required = true
    }

    if (attr.validate) {
        convertValidate(attr.validate, result)
    }

    return result
}

const convertValidate = (validate, result) => {
    // limit
    if (validate.min) {
        result.min = validate.min
    }
    if (validate.max) {
        result.max = validate.max
    }
    if (validate.len) {
        if (validate.len instanceof Array) {
            result.min = validate.len[0]
            if (result.max > validate.len[1])
                result.max = validate.len[1]
        }
        else if (typeof validate.len === 'number') {
            if (result.max > validate.len)
                result.max = validate.len
        }
    }

    // enum
    if (validate.isIn) {
        if (validate.isIn.length >= 1 && validate.isIn[0].length) {
            result.enum = validate.isIn[0]
        }
    }
    if (validate.notIn) {
        if (validate.notIn.length >= 1 && validate.notIn[0].length) {
            result.not = {
                enum: validate.notIn[0]
            }
        }
    }

    // pattern
    if (validate.is) {
        if (validate.is instanceof RegExp) {
            result.pattern = validate.is.source
        }
        else if (validate.is instanceof Array) {
            result.pattern = validate.is[0]
        }
    }
    if (validate.not) {
        if (validate.not instanceof RegExp) {
            result.not = {
                pattern: validate.not.source
            }
        }
        else if (validate.not instanceof Array) {
            result.not = {
                pattern: validate.not[0]
            }
        }
    }

    // more pattern
    if (validate.isAlpha) {
        result.pattern = '^[a-zA-Z]+$'
    }
    if (validate.isAlphanumeric) {
        result.pattern = '^[a-zA-Z0-9]+$'
    }
    if (validate.isNumeric || validate.isFloat || validate.isDecimal) {
        result.pattern = '^[+-]?([0-9]*[.])?[0-9]+$'
    }
    if (validate.isInt) {
        result.pattern = '^[+-]?[0-9]+$'
    }
    if (validate.notEmpty) {
        result.not = {
            const: ''
        }
    }
}

const precisionToValue = (precision, digit) => {
    let value = 9
    for (let i = 1; i < precision; i++) {
        value *= 10
        value += 9
    }

    if (digit > 0 && digit <= precision) {
        for (let i = 0; i < digit; i++) {
            value /= 10
        }
    }

    return value
}

// ----------------------------------------------------------------------

module.exports = {
    to,
    toFile,
    toJs
}
