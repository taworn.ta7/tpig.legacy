'use strict'
const path = require('path')
const convert = require('sequelize-models-to-joi')
const db = require('./db')

// running
const run = async () => {
    try {
        await db.sequelize.sync({ force: true })

        convert.to(path.join(__dirname, 'schemas.outputs'), [
            { model: db.Strings, file: 'Strings.js' },
            { model: db.Numerics, file: 'Numerics.js' }
        ], { useRequired: true })
        convert.to(path.join(__dirname, 'schemas.outputs'), [
            { model: db.Dates, file: 'Dates.js' }
        ], { useRequired: false })
        convert.to(path.join(__dirname, 'schemas.outputs'), [
            { model: db.Others, file: 'Others.js' }
        ])

        process.exit(0)
    }
    catch (ex) {
        console.log(ex)
        process.exit(1)
    }
}
run()
