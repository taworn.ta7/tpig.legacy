Numerics: Joi.object({

	id: Joi.number().integer()
		.required(),

	bool: Joi.boolean()
		.allow(null),

	integer: Joi.number().integer()
		.allow(null),

	int5: Joi.number().integer()
		.min(-99999).max(99999)
		.allow(null),

	bigInt: Joi.number().integer()
		.allow(null),

	float: Joi.number()
		.allow(null),

	float5: Joi.number()
		.min(-99999).max(99999)
		.allow(null),

	float5_2: Joi.number()
		.min(-999.99).max(999.99)
		.allow(null),

	double: Joi.number()
		.allow(null),

	decimal: Joi.number()
		.allow(null),

	decimal10: Joi.number()
		.min(-9999999999).max(9999999999)
		.allow(null),

	decimal10_2: Joi.number()
		.min(-99999999.99).max(99999999.99)
		.allow(null),

	limit: Joi.number().integer()
		.min(5).max(55555)
		.allow(null),

	createdAt: Joi.date().iso()
		.required(),

	updatedAt: Joi.date().iso()
		.required(),

})