Others: Joi.object({

	id: Joi.number().integer(),

	uuid: Joi.string()
		.pattern(/^[a-zA-Z0-9+\-*/@#$%]+$/)
		.allow(null, ''),

	enum: Joi.string()
		.valid("a","b","c")
		.allow(null, ''),

	createdAt: Joi.date().iso(),

	updatedAt: Joi.date().iso(),

})