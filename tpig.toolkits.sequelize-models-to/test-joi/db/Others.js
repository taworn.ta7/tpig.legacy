'use strict'
const { DataTypes } = require('sequelize')
const db = require('./sequelize')

module.exports = db.define('Others', {
    uuid: DataTypes.UUID,
    enum: {
        type: DataTypes.ENUM,
        values: ['a', 'b', 'c']
    }
}, {
    tableName: 'to-joi-others'
})
