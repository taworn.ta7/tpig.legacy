'use strict'
const { DataTypes } = require('sequelize')
const db = require('./sequelize')

module.exports = db.define('Strings', {
    string: {
        type: DataTypes.STRING,
        allowNull: false
    },
    string1234: DataTypes.STRING(1234),
    stringBinary: DataTypes.STRING.BINARY,

    textTiny: DataTypes.TEXT('tiny'),
    text: DataTypes.TEXT(),
    textMedium: DataTypes.TEXT('medium'),
    textLong: DataTypes.TEXT('long'),

    limit: {
        type: DataTypes.TEXT('long'),
        validate: {
            len: 5
        }
    },
    limit2: {
        type: DataTypes.TEXT('long'),
        validate: {
            len: [1, 5]
        }
    },

    enum: {
        type: DataTypes.STRING,
        validate: {
            isIn: [['a', 'b', 'c']]
        }
    },
    enumNot: {
        type: DataTypes.STRING,
        validate: {
            notIn: [['a', 'b', 'c']]
        }
    },

    pattern: {
        type: DataTypes.STRING,
        validate: {
            is: /^[a-zA-Z0-9]*$/
        }
    },
    patternNot: {
        type: DataTypes.STRING,
        validate: {
            not: /[a-zA-Z0-9]/
        }
    },

    isAlpha: {
        type: DataTypes.STRING(10),
        validate: {
            isAlpha: true,
        }
    },
    isAlphanumeric: {
        type: DataTypes.STRING(10),
        validate: {
            isAlphanumeric: true,
        }
    },
    isInt: {
        type: DataTypes.STRING(10),
        validate: {
            isInt: true,
        }
    },
    isFloat: {
        type: DataTypes.STRING(10),
        validate: {
            isFloat: true,
        }
    },

    notEmpty: {
        type: DataTypes.STRING(10),
        validate: {
            notEmpty: true,
        }
    }
}, {
    tableName: 'to-joi-strings'
})
