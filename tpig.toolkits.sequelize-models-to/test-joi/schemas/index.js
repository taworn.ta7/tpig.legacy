'use strict'
module.exports = {
    string: require('./Strings'),
    numeric: require('./Numerics'),
    date: require('./Dates'),
    other: require('./Others')
}
