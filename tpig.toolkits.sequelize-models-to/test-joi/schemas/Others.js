'use strict'
const Joi = require('joi')

const _ = Joi.object({

	uuid: Joi.string()
		.pattern(/^[a-zA-Z0-9+\-*/@#$%]+$/)
		.allow(null, ''),

	enum: Joi.string()
		.valid("a", "b", "c")
		.allow(null, ''),

})

module.exports = _
