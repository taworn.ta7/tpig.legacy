'use strict'
const Joi = require('joi')

const _ = Joi.object({

	date: Joi.date().iso()
		.allow(null),

	dateOnly: Joi.date().iso()
		.allow(null),

})

module.exports = _
