'use strict'
const Joi = require('joi')

const _ = Joi.object({

	string: Joi.string()
		.max(255)
		.allow('').required(),

	string1234: Joi.string()
		.max(1234)
		.allow(null, ''),

	stringBinary: Joi.string()
		.max(255)
		.allow(null, ''),

	textTiny: Joi.string()
		.max(255)
		.allow(null, ''),

	text: Joi.string()
		.max(65535)
		.allow(null, ''),

	textMedium: Joi.string()
		.max(16777215)
		.allow(null, ''),

	textLong: Joi.string()
		.max(4294967295)
		.allow(null, ''),

	limit: Joi.string()
		.max(5)
		.allow(null, ''),

	limit2: Joi.string()
		.min(1).max(5)
		.allow(null, ''),

	enum: Joi.string()
		.max(255)
		.valid("a", "b", "c")
		.allow(null, ''),

	enumNot: Joi.string()
		.max(255)
		.allow(null, ''),

	pattern: Joi.string()
		.max(255)
		.pattern(/^[a-zA-Z0-9]*$/)
		.allow(null, ''),

	patternNot: Joi.string()
		.max(255)
		.allow(null, ''),

	isAlpha: Joi.string()
		.max(10)
		.pattern(/^[a-zA-Z]+$/)
		.allow(null, ''),

	isAlphanumeric: Joi.string()
		.max(10)
		.pattern(/^[a-zA-Z0-9]+$/)
		.allow(null, ''),

	isInt: Joi.string()
		.max(10)
		.pattern(/^[+-]?[0-9]+$/)
		.allow(null, ''),

	isFloat: Joi.string()
		.max(10)
		.pattern(/^[+-]?([0-9]*[.])?[0-9]+$/)
		.allow(null, ''),

	notEmpty: Joi.string()
		.max(10)
		.allow(null, ''),

})

module.exports = _
