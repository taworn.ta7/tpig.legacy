'use strict'
const schemas = require('../schemas')

describe("testing Others schema", () => {

    test("uuid: error", () => {
        const result = schemas.other.validate({
            uuid: 0
        })
        expect(result.error).not.toBe(null)
    })

    test("uuid: invalid", () => {
        const result = schemas.other.validate({
            uuid: 'ab cd'
        })
        expect(result.error).not.toBe(null)
    })

    test("uuid: ok", () => {
        const result = schemas.other.validate({
            uuid: 'abcd+WXYZ-1234*5678/@#$%'
        })
        expect(result.error).toBe(undefined)
    })

    // ----------------------------------------------------------------------

    test("enum: invalid", () => {
        const result = schemas.other.validate({
            enum: 'z'
        })
        expect(result.error).not.toBe(null)
    })

    test("enum: ok", () => {
        const result = schemas.other.validate({
            enum: 'a'
        })
        expect(result.error).toBe(undefined)
    })

})
