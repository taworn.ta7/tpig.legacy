'use strict'
const schemas = require('../schemas')

describe("testing Dates schema", () => {

    test("date: error", () => {
        const result = schemas.date.validate({
            date: 0
        })
        expect(result.error).not.toBe(null)
    })

    test("date: invalid", () => {
        const result = schemas.date.validate({
            date: '2020-13-31T11:22:33.4444Z'
        })
        expect(result.error).not.toBe(null)
    })

    test("date: ok", () => {
        const result = schemas.date.validate({
            date: '2020-12-31T11:22:33.4444Z'
        })
        expect(result.error).toBe(undefined)
    })

    // ----------------------------------------------------------------------

    test("date only: error", () => {
        const result = schemas.date.validate({
            dateOnly: 0
        })
        expect(result.error).not.toBe(null)
    })

    test("date only: invalid", () => {
        const result = schemas.date.validate({
            dateOnly: '2020-13-31'
        })
        expect(result.error).not.toBe(null)
    })

    test("date only: valid but have time", () => {
        const result = schemas.date.validate({
            dateOnly: '2020-12-31T11:22:33.4444Z'
        })
        expect(result.error).not.toBe(null)
    })

    test("date only: ok", () => {
        const result = schemas.date.validate({
            dateOnly: '2020-12-31'
        })
        expect(result.error).toBe(undefined)
    })

})
