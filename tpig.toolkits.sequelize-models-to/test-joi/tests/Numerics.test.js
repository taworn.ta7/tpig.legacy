'use strict'
const schemas = require('../schemas')

describe("testing Numerics schema", () => {

    test("bool: true or false only", () => {
        const result = schemas.numeric.validate({
            bool: 0
        })
        expect(result.error).not.toBe(null)
    })

    test("bool: ok", () => {
        const result = schemas.numeric.validate({
            bool: true
        })
        expect(result.error).toBe(undefined)
    })

    // ----------------------------------------------------------------------

    test("precision: break min", () => {
        const result = schemas.numeric.validate({
            int5: -999999
        })
        expect(result.error).not.toBe(null)
    })

    test("precision: break max", () => {
        const result = schemas.numeric.validate({
            int5: 999999
        })
        expect(result.error).not.toBe(null)
    })

    test("precision: ok", () => {
        const result = schemas.numeric.validate({
            int5: 55555
        })
        expect(result.error).toBe(undefined)
    })

    // ----------------------------------------------------------------------

    test("precision decimal: break min", () => {
        const result = schemas.numeric.validate({
            decimal10_2: -99999999.999
        })
        expect(result.error).not.toBe(null)
    })

    test("precision decimal: break max", () => {
        const result = schemas.numeric.validate({
            decimal10_2: 99999999.999
        })
        expect(result.error).not.toBe(null)
    })

    test("precision decimal: ok", () => {
        const result = schemas.numeric.validate({
            decimal10_2: 99999999.99
        })
        expect(result.error).toBe(undefined)
    })

    // ----------------------------------------------------------------------

    test("limit: break min", () => {
        const result = schemas.numeric.validate({
            limit: 0
        })
        expect(result.error).not.toBe(null)
    })

    test("limit: break max", () => {
        const result = schemas.numeric.validate({
            limit: 66666
        })
        expect(result.error).not.toBe(null)
    })

    test("limit: ok", () => {
        const result = schemas.numeric.validate({
            limit: 555
        })
        expect(result.error).toBe(undefined)
    })

})
