'use strict'
const schemas = require('../schemas')

describe("testing Strings schema", () => {

    test("limit: break maxLength", () => {
        const result = schemas.string.validate({
            string: "",
            limit: "John Doe"
        })
        expect(result.error).not.toBe(null)
    })

    test("limit: break maxLength", () => {
        const result = schemas.string.validate({
            string: "",
            limit2: "John Doe"
        })
        expect(result.error).not.toBe(null)
    })

    test("limit: break minLength", () => {
        const result = schemas.string.validate({
            string: "",
            limit2: ""
        })
        expect(result.error).not.toBe(null)
    })

    test("limit: ok", () => {
        const result = schemas.string.validate({
            string: "",
            limit2: "John"
        })
        expect(result.error).toBe(undefined)
    })

    // ----------------------------------------------------------------------

    test("enum: invalid string", () => {
        const result = schemas.string.validate({
            string: "",
            enum: "d"
        })
        expect(result.error).not.toBe(null)
    })

    test("enum: ok", () => {
        const result = schemas.string.validate({
            string: "",
            enum: "c"
        })
        expect(result.error).toBe(undefined)
    })

    /*
    test("enum not: invalid string", () => {
        const result = schemas.string.validate({
            string: "",
            enumNot: "a"
        })
        expect(result.error).not.toBe(null)
    })

    test("enum not: ok", () => {
        const result = schemas.string.validate({
            string: "",
            enumNot: "z"
        })
        expect(result.error).toBe(undefined)
    })
    */

    // ----------------------------------------------------------------------

    test("pattern: invalid character(s)", () => {
        const result = schemas.string.validate({
            string: "",
            pattern: "+-*/"
        })
        expect(result.error).not.toBe(null)
    })

    test("pattern: ok", () => {
        const result = schemas.string.validate({
            string: "",
            pattern: "abcdefghij0123456789"
        })
        expect(result.error).toBe(undefined)
    })

    /*
    test("pattern not: invalid character(s)", () => {
        const result = schemas.string.validate({
            string: "",
            patternNot: "abcdefghij0123456789"
        })
        expect(result.error).not.toBe(null)
    })

    test("pattern not: ok", () => {
        const result = schemas.string.validate({
            string: "",
            patternNot: "+-* /"
        })
        expect(result.error).toBe(undefined)
    })
    */

    // ----------------------------------------------------------------------

    test("isAlpha: invalid character(s)", () => {
        const result = schemas.string.validate({
            string: "",
            isAlpha: "abcABC012"
        })
        expect(result.error).not.toBe(null)
    })

    test("isAlpha: ok", () => {
        const result = schemas.string.validate({
            string: "",
            isAlpha: "abcABC"
        })
        expect(result.error).toBe(undefined)
    })

    test("isAlphanumeric: invalid character(s)", () => {
        const result = schemas.string.validate({
            string: "",
            isAlphanumeric: "abcABC012+-*/"
        })
        expect(result.error).not.toBe(null)
    })

    test("isAlphanumeric: ok", () => {
        const result = schemas.string.validate({
            string: "",
            isAlphanumeric: "abcABC012"
        })
        expect(result.error).toBe(undefined)
    })

    // ----------------------------------------------------------------------

    test("isInt: invalid character(s)", () => {
        const result = schemas.string.validate({
            string: "",
            isInt: "+012.0"
        })
        expect(result.error).not.toBe(null)
    })

    test("isInt: ok", () => {
        const result = schemas.string.validate({
            string: "",
            isInt: "-012"
        })
        expect(result.error).toBe(undefined)
    })

    test("isFloat: invalid character(s)", () => {
        const result = schemas.string.validate({
            string: "",
            isFloat: "+012.0.0"
        })
        expect(result.error).not.toBe(null)
    })

    test("isFloat: ok", () => {
        const result = schemas.string.validate({
            string: "",
            isFloat: "-012.0"
        })
        expect(result.error).toBe(undefined)
    })

    // ----------------------------------------------------------------------

    test("notEmpty: empty error", () => {
        const result = schemas.string.validate({
            string: "",
            notEmpty: ""
        })
        expect(result.error).not.toBe(null)
    })

    test("notEmpty: ok", () => {
        const result = schemas.string.validate({
            string: "",
            notEmpty: "^-^"
        })
        expect(result.error).toBe(undefined)
    })

    // ----------------------------------------------------------------------

    test("required: empty error", () => {
        const result = schemas.string.validate({
        })
        expect(result.error).not.toBe(null)
    })

})
