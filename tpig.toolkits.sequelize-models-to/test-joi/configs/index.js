'use strict'
const path = require('path')
const Config = require('merge-config-ex')
const config = new Config()

config.file(path.join(__dirname, 'config.yaml'))

module.exports = config
