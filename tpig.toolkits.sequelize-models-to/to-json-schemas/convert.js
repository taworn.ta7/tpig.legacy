'use strict'
const path = require('path')
const fs = require('fs')

/*
 * to(outputDir, list)
 * convert sequelize model to JSON schema files
 */
const to = (outputDir, list) => {
    for (let i = 0; i < list.length; i++) {
        const item = list[i]
        toFile(item.model, path.join(outputDir, item.file))
    }
}

/*
 * toFile(model, file)
 * convert sequelize model to JSON schema file
 */
const toFile = (model, file) => {
    const schema = toJs(model)
    const text = JSON.stringify(schema, null, 4)
    fs.writeFileSync(file, text)
}

/*
 * toJs(model)
 * convert sequelize model to JSON schema JS object
 */
const toJs = (model) => {
    const properties = {}
    const required = []
    for (let key in model.rawAttributes) {
        const attr = model.rawAttributes[key]
        properties[key] = convertProperty(attr)
        if (attr.allowNull === false)
            required.push(key)
    }

    const schema = {
        [model.name]: {
            type: 'object',
            properties,
            required
        }
    }
    return schema
}

// ----------------------------------------------------------------------

const convertProperty = (attr) => {
    const result = {}

    switch (attr.type.key) {
        case 'STRING':
            result.type = 'string'
            result.maxLength = attr.type._length
            break

        case 'TEXT':
            result.type = 'string'
            if (typeof attr.type.options.length === 'undefined')
                result.maxLength = 0xffff
            else {
                const size = attr.type.options.length.toLowerCase()
                if (size === 'tiny')
                    result.maxLength = 0xff
                if (size === '')
                    result.maxLength = 0xffff
                else if (size === 'medium')
                    result.maxLength = 0xffffff
                else if (size === 'long')
                    result.maxLength = 0xffffffff
            }
            break

        case 'BOOLEAN':
            result.type = 'boolean'
            break

        case 'INTEGER':
        case 'BIGINT':
            result.type = 'integer'
            if (typeof attr.type.options.length !== 'undefined') {
                const value = precisionToValue(attr.type.options.length)
                result.minimum = -value
                result.maximum = value
            }
            break

        case 'REAL':
        case 'FLOAT':
        case 'DOUBLE':
        case 'DOUBLE PRECISION':
            result.type = 'number'
            if (typeof attr.type.options.length !== 'undefined') {
                const value = precisionToValue(attr.type.options.length, attr.type.options.decimals)
                result.minimum = -value
                result.maximum = value
            }
            break

        case 'DECIMAL':
            result.type = 'number'
            if (typeof attr.type.options.precision !== 'undefined') {
                const value = precisionToValue(attr.type.options.precision, attr.type.options.scale)
                result.minimum = -value
                result.maximum = value
            }
            break

        case 'DATE':
            result.type = 'string'
            result.format = 'date-time'
            break

        case 'DATEONLY':
            result.type = 'string'
            result.format = 'date'
            break

        case 'UUID':
            result.type = 'string'
            result.pattern = '^[a-zA-Z0-9+\\-*/@#$%]+$'
            break

        case 'ENUM':
            result.type = 'string'
            result.enum = attr.values
            break
    }

    if (typeof attr.allowNull === 'undefined' || attr.allowNull) {
        result.type = [result.type, 'null']
    }

    if (attr.validate) {
        convertValidate(attr.validate, result)
    }

    return result
}

const convertValidate = (validate, result) => {
    // limit
    if (validate.min) {
        result.minimum = validate.min
    }
    if (validate.max) {
        result.maximum = validate.max
    }
    if (validate.len) {
        if (validate.len instanceof Array) {
            result.minLength = validate.len[0]
            if (result.maxLength > validate.len[1])
                result.maxLength = validate.len[1]
        }
        else if (typeof validate.len === 'number') {
            if (result.maxLength > validate.len)
                result.maxLength = validate.len
        }
    }

    // enum
    if (validate.isIn) {
        if (validate.isIn.length >= 1 && validate.isIn[0].length) {
            result.enum = validate.isIn[0]
        }
    }
    if (validate.notIn) {
        if (validate.notIn.length >= 1 && validate.notIn[0].length) {
            result.not = {
                enum: validate.notIn[0]
            }
        }
    }

    // pattern
    if (validate.is) {
        if (validate.is instanceof RegExp) {
            result.pattern = validate.is.source
        }
        else if (validate.is instanceof Array) {
            result.pattern = validate.is[0]
        }
    }
    if (validate.not) {
        if (validate.not instanceof RegExp) {
            result.not = {
                pattern: validate.not.source
            }
        }
        else if (validate.not instanceof Array) {
            result.not = {
                pattern: validate.not[0]
            }
        }
    }

    // more pattern
    if (validate.isAlpha) {
        result.pattern = '^[a-zA-Z]+$'
    }
    if (validate.isAlphanumeric) {
        result.pattern = '^[a-zA-Z0-9]+$'
    }
    if (validate.isNumeric || validate.isFloat || validate.isDecimal) {
        result.pattern = '^[+-]?([0-9]*[.])?[0-9]+$'
    }
    if (validate.isInt) {
        result.pattern = '^[+-]?[0-9]+$'
    }
    if (validate.notEmpty) {
        result.not = {
            const: ''
        }
    }
}

const precisionToValue = (precision, digit) => {
    let value = 9
    for (let i = 1; i < precision; i++) {
        value *= 10
        value += 9
    }

    if (digit > 0 && digit <= precision) {
        for (let i = 0; i < digit; i++) {
            value /= 10
        }
    }

    return value
}

// ----------------------------------------------------------------------

module.exports = {
    to,
    toFile,
    toJs
}
