# sequelize-models-to-json-schemas

Sequelize models convert to JSON schemas.

# Using

Please download full packages at [GitLab](https://gitlab.com/taworn.ta7/tpig.toolkits.sequelize-models-to) and look at [test-json-schemas](https://gitlab.com/taworn.ta7/tpig.toolkits.sequelize-models-to/-/tree/master/test-json-schemas) directory.

# Thank You

* sequelize, https://sequelize.org
* ajv, https://ajv.js.org
* gitignore, https://github.com/github/gitignore and https://gitignore.io

# Last

Sorry, but I'm not good at English. T_T

Taworn Ta.
