module.exports = {
    verbose: true,
    testMatch: [
        "**/tests/**/*.[jt]s?(x)", "**/?(*.)+(spec|test).[jt]s?(x)"
    ]
}
