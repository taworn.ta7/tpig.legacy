This package is legacy!

# sequelize-models-to...

Sequelize models convert to [JSON schemas](./to-json-schemas/README.md) and/or [Joi](./to-joi/README.md).

# Motivation

I've write 'node' and I found myself that, sometime, JSON schemas cannot convert from sequelize models directly.  So, I write program to convert sequelize models and then convert them to 'fragment code'.  After generate, it's will convert database fields to partial JSON schemas, copy & paste, and shape your program. 

# Requirements

* Node
* Sequelize
* Ajv
* Joi

# Thank You

* sequelize, https://sequelize.org
* ajv, https://ajv.js.org
* joi, https://joi.dev
* gitignore, https://github.com/github/gitignore and https://gitignore.io

# Last

Sorry, but I'm not good at English. T_T

Taworn Ta.
