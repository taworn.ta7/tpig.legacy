﻿using System.Windows;
using System.Windows.Controls;
using NLog;
using Tpig.Components.Localization;
using Tpig.Components.Navigators;
using Tpig.Examples.Ui;

namespace Tpig.Examples.Advance {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public MainWindow() {
            InitializeComponent();
            LocaleManager.Instance.Current = "en";
            Localize.Load(this);

            SetupUi("NavigateMap0", Container0, Popup0);
            SetupUi("NavigateMap1", Container1, Popup1);
        }

        // ----------------------------------------------------------------------

        private void Navigating(ControlNode prev, ControlNode next) {
            logger.Trace("navigating: {0} => {1}", prev.Name, next.Name);
        }

        private void Navigated(ControlNode prev, ControlNode next) {
            logger.Trace("navigated: {0} => {1}", prev.Name, next.Name);
        }

        // ----------------------------------------------------------------------

        private void SetupUi(string navigateKey, Panel container, Panel popup) {
            var controlMap = new ControlMap();
            controlMap.Add("OneControl", () => new OneControl(navigateKey));
            controlMap.Add("TwoControl", () => new TwoControl(navigateKey));
            controlMap.Add("ThreeControl", () => new ThreeControl(navigateKey));
            controlMap.Add("FourControl", () => new FourControl(navigateKey), "TwoControl");

            var navigateMap = new NavigateMap(container, controlMap, "OneControl");
            navigateMap.Navigating += Navigating;
            navigateMap.Navigated += Navigated;
            Application.Current.Properties[navigateKey] = navigateMap;

            var popupHost = new PopupHost(container, popup);
            Application.Current.Properties[navigateKey + "/Popup"] = popupHost;
        }

    }
}
