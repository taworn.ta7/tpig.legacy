﻿using System.Windows;
using System.Windows.Controls;
using NLog;
using Tpig.Components.Localization;
using Tpig.Components.Navigators;

namespace Tpig.Examples.Ui {
    /// <summary>
    /// Interaction logic for ThreeControl.xaml
    /// </summary>
    public partial class ThreeControl : UserControl, IWindowExtensions {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private string navigateKey = null;
        private string name = null;

        public ThreeControl(string navigateKey) {
            InitializeComponent();
            Localize.Load(this);
            this.navigateKey = navigateKey;
        }

        // ----------------------------------------------------------------------

        public void Enter(string name) {
            logger.Trace("{0}: enter", name);
            this.name = name;
        }

        public void Leave(string name) {
            logger.Trace("{0}: leave", name);
            this.name = null;
        }

        // ----------------------------------------------------------------------

        private void Back(object sender, RoutedEventArgs e) {
            var parameter = (sender as Button)?.CommandParameter as string;
            CommonControl.Back(navigateKey, name, parameter);
        }

        private void Next(object sender, RoutedEventArgs e) {
            var message = Localize.Text(this, "Message_Three");
            MessageBox.Show(Window.GetWindow(this), message, "WARNING!", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void ChangeLocale(object sender, RoutedEventArgs e) {
            var parameter = (sender as Button)?.CommandParameter as string;
            CommonControl.ChangeLocale(name, parameter);
        }

        private void Exit(object sender, RoutedEventArgs e) {
            CommonControl.Exit(name, this);
        }

        // ----------------------------------------------------------------------

        private void Center(object sender, RoutedEventArgs e) {
            var message = Localize.Text(this, "Message_Three");
            MessageBox.Show(Window.GetWindow(this), message, "WARNING!", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

    }
}
