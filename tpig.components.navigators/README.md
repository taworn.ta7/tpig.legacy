**Tpig.Components.Navigators**
==============================

.NET WPF components for back/next navigations.


## Examples

- Tpig.Examples.Ui: A shared UserControl's.
- Tpig.Examples.Basic: Basic of usage.
- Tpig.Examples.Advance: More of features.


## Requirements

- .NET version 6.0


## Thank You

- NLog, https://nlog-project.org
- gitignore, https://github.com/github/gitignore and https://gitignore.io


## NuGet

You can retrieve package at https://www.nuget.org/packages/Tpig.Components.Navigators.


## Last

Sorry, but I'm not good at English. T_T

Taworn Ta.

