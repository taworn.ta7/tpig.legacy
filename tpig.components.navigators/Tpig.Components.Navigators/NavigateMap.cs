﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Tpig.Components.Navigators {
    /// <summary>
    /// A navigation map.
    /// </summary>
    public class NavigateMap {

        /// <summary>
        /// Constructor.
        /// </summary>
        public NavigateMap(Panel parent, ControlMap map, string nameToStart) {
            Parent = parent;
            Map = map;
            controlDict = new Dictionary<string, UserControl>();
            CurrentNode = Map.Find(nameToStart);

            if (CurrentNode == null)
                throw new Exception("Current node is null!  This must be your mistake, check ControlMap must have children.");

            var control = NameToControl(CurrentNode.Name);
            control.Visibility = Visibility.Visible;
            control.Focus();
            (control as IWindowExtensions)?.Enter(CurrentNode.Name);
        }

        // ----------------------------------------------------------------------

        /// <summary>
        /// A parent control.
        /// </summary>
        public Panel Parent { get; private set; } = null;

        /// <summary>
        /// A map to guide navigation.
        /// </summary>
        public ControlMap Map { get; private set; } = null;

        /// <summary>
        /// Data structure to keep UserControl created.
        /// </summary>
        private Dictionary<string, UserControl> controlDict = null;

        // ----------------------------------------------------------------------

        /// <summary>
        /// Current node.
        /// </summary>
        public ControlNode CurrentNode { get; private set; } = null;

        /// <summary>
        /// Move back by ControlMap.
        /// </summary>
        public ControlNode Back(string name = null) {
            var link = (ControlNode)null;
            if (string.IsNullOrWhiteSpace(name))
                link = CurrentNode.BackLink;
            else
                link = Map.Find(name);
            if (link != null) {
                Navigate(link, Direction.Back);
                return link;
            }
            else
                return null;
        }

        /// <summary>
        /// Move next by ControlMap.
        /// </summary>
        public ControlNode Next(string name = null) {
            var link = (ControlNode)null;
            if (string.IsNullOrWhiteSpace(name))
                link = CurrentNode.NextLink;
            else
                link = Map.Find(name);
            if (link != null) {
                Navigate(link, Direction.Next);
                return link;
            }
            else
                return null;
        }

        // ----------------------------------------------------------------------

        /// <summary>
        /// Direction which have Default, Back and Next.
        /// </summary>
        public enum Direction {
            Default,
            Back,
            Next
        }

        /// <summary>
        /// A back and next animations.
        /// </summary>
        public delegate void AnimationType(FrameworkElement element0, FrameworkElement element1, Duration timeUsed, EventHandler handler0, EventHandler handler1);
        public AnimationType AnimationBack { get; set; } = Internals.Animate.SlideLeftToRightFade;
        public AnimationType AnimationNext { get; set; } = Internals.Animate.SlideRightToLeftFade;

        /// <summary>
        /// Before navigation event.
        /// Parameter 1 is node to leave.
        /// Parameter 2 is node to enter.
        /// </summary>
        public Action<ControlNode, ControlNode> Navigating { get; set; } = null;
        /// <summary>
        /// After navigation event.
        /// Parameter 1 is node to leave.
        /// Parameter 2 is node to enter.
        /// </summary>
        public Action<ControlNode, ControlNode> Navigated { get; set; } = null;

        /// <summary>
        /// Perform navigation.
        /// </summary>
        public void Navigate(ControlNode node, Direction dir = Direction.Default) {
            if (node != null && node != CurrentNode) {
                if (CurrentNode != null) {
                    var element0 = NameToControl(CurrentNode.Name);
                    var element1 = NameToControl(node.Name);
                    element1.Visibility = Visibility.Visible;

                    var animation = (AnimationType)null;
                    switch (dir) {
                        default:
                        case Direction.Default:
                            if (CurrentNode.Index < node.Index)
                                animation = AnimationNext;
                            else
                                animation = AnimationBack;
                            break;
                        case Direction.Back:
                            animation = AnimationBack;
                            break;
                        case Direction.Next:
                            animation = AnimationNext;
                            break;
                    }

                    var prevNode = CurrentNode;
                    var nextNode = node;
                    Navigating?.Invoke(prevNode, nextNode);
                    (element0 as IWindowExtensions)?.Leave(CurrentNode.Name);
                    Parent.Dispatcher.BeginInvoke(DispatcherPriority.Render, (Action)(() => {
                        animation(element0, element1, TimeSpan.FromMilliseconds(150),
                            (_0, _1) => element0.Visibility = Visibility.Hidden,
                            (_0, _1) => {
                                CurrentNode = node;
                                element1.Focus();
                                (element1 as IWindowExtensions)?.Enter(CurrentNode.Name);
                                Navigated?.Invoke(prevNode, nextNode);
                            });
                    }));
                }
            }
        }

        // ----------------------------------------------------------------------

        public UserControl NameToControl(string name) {
            if (controlDict.ContainsKey(name)) {
                return controlDict[name];
            }
            else {
                var node = Map.Find(name);
                var control = node.CreateControl();
                control.Visibility = Visibility.Hidden;
                Parent.Children.Add(control);
                controlDict.Add(name, control);
                return control;
            }
        }

    }
}
