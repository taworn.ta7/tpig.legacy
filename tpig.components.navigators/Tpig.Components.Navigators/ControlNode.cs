﻿using System.Windows.Controls;

namespace Tpig.Components.Navigators {
    /// <summary>
    /// A data node which act as way point in ControlMap.
    /// </summary>
    public class ControlNode {

        /// <summary>
        /// An unique index, must manage by ControlMap.
        /// </summary>
        public int Index { get; set; } = -1;

        /// <summary>
        /// A name which reference when call this node.
        /// </summary>
        public string Name { get; set; } = "";

        /// <summary>
        /// A default back link.
        /// </summary>
        public ControlNode BackLink { get; set; } = null;

        /// <summary>
        /// A default next link.
        /// </summary>
        public ControlNode NextLink { get; set; } = null;

        /// <summary>
        /// A create UserControl function.
        /// </summary>
        public delegate UserControl CreateControlType();
        public CreateControlType CreateControl { get; set; } = null;

    }
}
