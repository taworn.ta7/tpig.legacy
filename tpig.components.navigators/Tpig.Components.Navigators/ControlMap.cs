﻿using System;
using System.Collections.Generic;

namespace Tpig.Components.Navigators {
    /// <summary>
    /// Data structure to manage ControlNode(s).
    /// </summary>
    public class ControlMap {

        /// <summary>
        /// A list data to keep node(s).
        /// </summary>
        public List<ControlNode> List { get; private set; } = new List<ControlNode>();

        /// <summary>
        /// Name to node lookup data structure.
        /// </summary>
        private Dictionary<string, ControlNode> dictionary = new Dictionary<string, ControlNode>();

        /// <summary>
        /// Add new node.
        /// </summary>
        public void Add(string name, ControlNode.CreateControlType create, string back = null) {
            var found = List.Find(x => x.Name.Equals(name));
            if (found != null) {
                throw new ArgumentException(string.Format("{0} is already used!", name));
            }

            var backLink = (ControlNode)null;
            if (string.IsNullOrWhiteSpace(back))
                backLink = List.Count > 0 ? List[List.Count - 1] : null;
            else
                backLink = Find(back);

            var node = new ControlNode() {
                Index = List.Count,
                Name = name,
                BackLink = backLink,
                CreateControl = create
            };
            if (node.BackLink != null) {
                if (node.BackLink.NextLink == null)
                    node.BackLink.NextLink = node;
            }

            List.Add(node);
            dictionary.Add(node.Name, node);
        }

        /// <summary>
        /// Find node by name.
        /// </summary>
        public ControlNode Find(string name) {
            return List.Find(o => {
                if (o.Name != null)
                    return o.Name.Equals(name, StringComparison.CurrentCulture);
                else
                    return false;
            });
        }

    }
}
