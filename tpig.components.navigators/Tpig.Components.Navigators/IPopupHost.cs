﻿using System.Windows.Controls;

namespace Tpig.Components.Navigators {
    public interface IPopupHost {

        /// <summary>
        /// Check if any popup attaching or not.
        /// </summary>
        bool IsAttaching { get; }

        /// <summary>
        /// Attach popup into host.
        /// </summary>
        void Attach(UserControl popup);

        /// <summary>
        /// Detach popup from host.
        /// </summary>
        void Detach();

    }
}
