﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Tpig.Components.Navigators {
    public class PopupHost : IPopupHost {

        /// <summary>
        /// Constructor.
        /// </summary>
        public PopupHost(Panel container, Panel popup) {
            Container = container;
            Popup = popup;
        }

        // ----------------------------------------------------------------------

        public Panel Container {
            get;
            private set;
        } = null;

        public Panel Popup {
            get;
            private set;
        } = null;

        // ----------------------------------------------------------------------

        /// <summary>
        /// An open and close animations.
        /// </summary>
        public delegate void AnimationType(FrameworkElement element, Duration timeUsed, EventHandler handler);
        public AnimationType AnimationIn { get; set; } = Internals.Animate.FadeIn;
        public AnimationType AnimationOut { get; set; } = Internals.Animate.FadeOut;

        /// <summary>
        /// Open popup event.
        /// </summary>
        public Action<FrameworkElement> PopupOpened { get; set; } = null;
        /// <summary>
        /// Close popup event.
        /// </summary>
        public Action<FrameworkElement> PopupClosed { get; set; } = null;

        // ----------------------------------------------------------------------

        public bool IsAttaching {
            get => Popup.Children.Count > 0;
        }

        public void Attach(UserControl popup) {
            if (Popup.Children.Count >= 1)
                throw new InvalidOperationException("PopupHostHelper support only 1 popup!");

            Container.IsEnabled = false;
            Popup.Children.Add(popup);
            Popup.Dispatcher.BeginInvoke(DispatcherPriority.Render, (Action)(() => {
                AnimationIn(popup, TimeSpan.FromMilliseconds(150), (_0, _1) => {
                    PopupOpened?.Invoke(popup);
                    popup.Focus();
                });
            }));
        }

        public void Detach() {
            if (IsAttaching) {
                var popup = Popup.Children[Popup.Children.Count - 1] as FrameworkElement;
                Popup.Dispatcher.BeginInvoke(DispatcherPriority.Render, (Action)(() => {
                    AnimationOut(popup, TimeSpan.FromMilliseconds(150), (_0, _1) => {
                        Popup.Children.Remove(popup);
                        Container.IsEnabled = true;
                        PopupClosed?.Invoke(popup);
                    });
                }));
            }
        }

    }
}
