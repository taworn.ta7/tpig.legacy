﻿namespace Tpig.Components.Navigators {
    public interface IWindowExtensions {

        /// <summary>
        /// Call when control is entering.
        /// </summary>
        void Enter(string name);

        /// <summary>
        /// Call when control is leaving.
        /// </summary>
        void Leave(string name);

    }
}
